var searchData=
[
  ['parse_660',['parse',['../classRequestHeader.html#ae7f8bea60ec86539bfb38d1378ffa1ba',1,'RequestHeader']]],
  ['player_661',['Player',['../classPlayer.html#a9ff0521ec7ac4544b96589778efb3f39',1,'Player::Player(std::string userName, int id, int points, std::vector&lt; int &gt; hand)'],['../classPlayer.html#a715bef7231214d7f63efe184f4493262',1,'Player::Player(const QJsonObject &amp;o)'],['../classPlayer.html#affe0cc3cb714f6deb4e62f0c0d3f1fd8',1,'Player::Player()']]],
  ['playerchoice_662',['playerChoice',['../classGame.html#a1d41d77fc4fa2cf4519a9f4142b96ab9',1,'Game']]],
  ['players_663',['players',['../classGame.html#ae35bdf620923ae041bfe63e06c13b69b',1,'Game']]],
  ['playervote_664',['playerVote',['../classGame.html#ae3c5f445a0b28b57542e10a3e4d1565c',1,'Game']]],
  ['points_665',['points',['../classPlayer.html#a5c58bd81dab558632f7e1a9b3fff0253',1,'Player']]],
  ['post_666',['POST',['../classNetworkService.html#acb7482a6e8ac040419fb8508784623c5',1,'NetworkService::POST()'],['../classMoxitServer.html#a7810af2b4e6f9703f039f6ff5c3cef8d',1,'MoxitServer::post()']]],
  ['processconnection_667',['processConnection',['../classMoxitServer.html#a20e0cc1f5bec89daf4a7ff46d13c41eb',1,'MoxitServer']]]
];
