var searchData=
[
  ['ui_392',['Ui',['../namespaceUi.html',1,'Ui'],['../classChat.html#ada2103e14285ec9dd588eca5e162aa31',1,'Chat::ui()'],['../classHostGame.html#ac9df8a453c12c3d95ea4087039f4b1e7',1,'HostGame::ui()'],['../classTable.html#a181a67fcb394951f06b0a01397386271',1,'Table::ui()'],['../classLobby.html#a8c196e21148d6ef7f120a8e12375d695',1,'Lobby::ui()'],['../classMainWindow.html#a35466a70ed47252a0191168126a352a5',1,'MainWindow::ui()'],['../classSettings.html#a98680be421d0ae7c34a57f2a159eacd4',1,'Settings::ui()']]],
  ['updatepoints_393',['updatePoints',['../classGame.html#a2ad48f77e98b18d96d8b5ea63cfa81b5',1,'Game']]],
  ['use_394',['use',['../classMoxitServer.html#aa435ab97288dd6a4997d59b304b91581',1,'MoxitServer']]],
  ['username_395',['username',['../classTable.html#aeb59c9598b663dbd23c4f5894be7e0e2',1,'Table::username()'],['../classSettings.html#aedf017ee354e0c8d03f94ba5d5dfa80a',1,'Settings::username()'],['../classPlayer.html#a42362a8da7abf0ba586c087c89ee899d',1,'Player::username()'],['../classSettingsService.html#aa41243b84e160e7e9ffc188aa3044a25',1,'SettingsService::username()']]]
];
