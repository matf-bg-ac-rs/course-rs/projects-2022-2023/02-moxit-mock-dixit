var searchData=
[
  ['narrator_5fchoice_274',['NARRATOR_CHOICE',['../game_8hpp.html#a896617de6e1c82953f407789633057d8ac52d0203e4390732df39b52244d67338',1,'game.hpp']]],
  ['narratorcard_275',['narratorCard',['../classTable.html#a58bfecd035557a900faf308745a2fbfa',1,'Table::narratorCard()'],['../classGame.html#aeb7a4244a7f4100fdbcffcaef0324c42',1,'Game::narratorCard()']]],
  ['narratorchoice_276',['narratorChoice',['../classGame.html#accbaf77d64f3ccf581153aff8faaf5b0',1,'Game']]],
  ['narratorid_277',['narratorId',['../classTable.html#a5497417a82a0fb4a9e5394e198d2714c',1,'Table']]],
  ['narrcardchosen_278',['narrCardChosen',['../classTable.html#adeaa8e8c8ee75d391c22163278fe69b0',1,'Table']]],
  ['networkservice_279',['NetworkService',['../classNetworkService.html',1,'NetworkService'],['../classNetworkService.html#ae37f04d34a43632252cb6bb153b38caf',1,'NetworkService::NetworkService()'],['../classNetworkService.html#a622ad41d140fd68bb104cd4ba197c140',1,'NetworkService::NetworkService(NetworkService const &amp;)=delete'],['../classTable.html#a806efb77188491599819fc437c1e02a4',1,'Table::networkService()'],['../classLobby.html#a414c5a0f7dfae5155f192b1447c37271',1,'Lobby::networkService()']]],
  ['networkservice_2ecpp_280',['NetworkService.cpp',['../NetworkService_8cpp.html',1,'']]],
  ['networkservice_2ehpp_281',['NetworkService.hpp',['../NetworkService_8hpp.html',1,'']]],
  ['nopls_282',['noPls',['../classTable.html#a895afdbd968c9f21539054deb81a3dec',1,'Table']]],
  ['notify_283',['notify',['../classGame.html#a15a6bae5f92a346c16deb6a5076898d9',1,'Game']]],
  ['notifyplayers_284',['notifyPlayers',['../classGame.html#a3adaecee5a5ff4caa723a89bb2f861df',1,'Game']]],
  ['num_5fof_5fplayers_285',['num_of_players',['../classSettings.html#ad7dce9b7529fe84751ff9b0981122273',1,'Settings']]],
  ['numberofplayers_286',['numberOfPlayers',['../classSettingsService.html#ae15dcf73c0a09ab8e088ba4b80943b94',1,'SettingsService']]],
  ['numofplayers_287',['numOfPlayers',['../classGame.html#ad04ded59da6b52a0c5c29403a8abaaab',1,'Game']]]
];
