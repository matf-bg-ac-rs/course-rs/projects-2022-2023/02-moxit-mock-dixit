var searchData=
[
  ['narratorcard_634',['narratorCard',['../classGame.html#aeb7a4244a7f4100fdbcffcaef0324c42',1,'Game']]],
  ['narratorchoice_635',['narratorChoice',['../classGame.html#accbaf77d64f3ccf581153aff8faaf5b0',1,'Game']]],
  ['networkservice_636',['NetworkService',['../classNetworkService.html#ae37f04d34a43632252cb6bb153b38caf',1,'NetworkService::NetworkService()'],['../classNetworkService.html#a622ad41d140fd68bb104cd4ba197c140',1,'NetworkService::NetworkService(NetworkService const &amp;)=delete']]],
  ['notify_637',['notify',['../classGame.html#a15a6bae5f92a346c16deb6a5076898d9',1,'Game']]],
  ['notifyplayers_638',['notifyPlayers',['../classGame.html#a3adaecee5a5ff4caa723a89bb2f861df',1,'Game']]],
  ['numberofplayers_639',['numberOfPlayers',['../classSettingsService.html#ae15dcf73c0a09ab8e088ba4b80943b94',1,'SettingsService']]],
  ['numofplayers_640',['numOfPlayers',['../classGame.html#ad04ded59da6b52a0c5c29403a8abaaab',1,'Game']]]
];
