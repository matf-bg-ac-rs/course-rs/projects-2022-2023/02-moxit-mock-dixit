var searchData=
[
  ['acceptconnection_84',['acceptConnection',['../classMoxitServer.html#a0e4d00dacf1fddd87bb2bd2e93c7f979',1,'MoxitServer']]],
  ['addcard_85',['addCard',['../classPlayer.html#a5b003a2011e65677bd57a81caa6109d8',1,'Player::addCard()'],['../classTable.html#a92340805801983bc4af6dc9357a4360d',1,'Table::addCard()']]],
  ['addcardwidget_86',['addCardWidget',['../classTable.html#a9a4feeeaeb83209a2bc38cb0c84e2d68',1,'Table']]],
  ['addepollwriteevent_87',['addEpollWriteEvent',['../classMoxitServer.html#a7000acb21bf5fb4ae7e62473697382bd',1,'MoxitServer']]],
  ['addhandlers_88',['addHandlers',['../classMoxitServer.html#a6db04833cb6adf5c54c17005d73bec53',1,'MoxitServer']]],
  ['addmessage_89',['addMessage',['../classChatBackend.html#a0cc35c21252bdcb6dbacfff312625b07',1,'ChatBackend']]],
  ['addplayer_90',['addPlayer',['../classGame.html#a0879ea0ba66526dd5a2122907d9b9727',1,'Game::addPlayer(const QJsonObject &amp;config)'],['../classGame.html#a4ec82bc369af3c90c76c800e3c218922',1,'Game::addPlayer(Player &amp;p)'],['../classTable.html#a2c2ecc1487848d63a192921e59aee6c4',1,'Table::addPlayer()']]],
  ['addpoints_91',['addPoints',['../classPlayer.html#a42f4e9ab541735d4105f7f727db14c2d',1,'Player']]],
  ['amihost_92',['amIHost',['../classGameStateService.html#a8fca38486fee8a9d6eecff3a7aea9db9',1,'GameStateService']]],
  ['aminarrator_93',['amINarrator',['../classGameStateService.html#ac41ea5b69e55d35e2a02b1f062a76209',1,'GameStateService']]],
  ['api_5fendpoint_94',['API_ENDPOINT',['../classConfig.html#a7c8d622a452f6df38885608486e997f1',1,'Config']]],
  ['api_5fendpoint_5factive_5fgames_95',['API_ENDPOINT_ACTIVE_GAMES',['../classConfig.html#aab52d7112fb6ce753a5c022d2931aa48',1,'Config']]],
  ['api_5fendpoint_5fchat_96',['API_ENDPOINT_CHAT',['../classConfig.html#a141a025a3c5740359373180ab332b829',1,'Config']]],
  ['api_5fendpoint_5fcreate_5fgame_97',['API_ENDPOINT_CREATE_GAME',['../classConfig.html#a79d0537ea072c7f13b21d1c46cecee89',1,'Config']]],
  ['api_5fendpoint_5fgame_5fstate_98',['API_ENDPOINT_GAME_STATE',['../classConfig.html#a17823373d286e665f20107893f692001',1,'Config']]],
  ['api_5fendpoint_5fjoin_5fgame_99',['API_ENDPOINT_JOIN_GAME',['../classConfig.html#a0de521028c7f91e4781fbfc049113c6f',1,'Config']]],
  ['api_5fendpoint_5fnarrator_5fchoice_100',['API_ENDPOINT_NARRATOR_CHOICE',['../classConfig.html#aad257c68f09fca58d4ddfdcccafccf3c',1,'Config']]],
  ['api_5fendpoint_5fplayer_5fchoice_101',['API_ENDPOINT_PLAYER_CHOICE',['../classConfig.html#ac48d683f3b61ca6950e212bec86621dc',1,'Config']]],
  ['api_5fendpoint_5fplayer_5fvote_102',['API_ENDPOINT_PLAYER_VOTE',['../classConfig.html#a3636472dda4803eff661dcc0cbc5789d',1,'Config']]],
  ['api_5fendpoint_5fstart_5fgame_103',['API_ENDPOINT_START_GAME',['../classConfig.html#a4b12e4128da79037ce7c75b3ea31dfe1',1,'Config']]]
];
