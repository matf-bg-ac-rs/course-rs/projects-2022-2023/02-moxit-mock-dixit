var searchData=
[
  ['game_604',['Game',['../classGame.html#ad59df6562a58a614fda24622d3715b65',1,'Game']]],
  ['gameid_605',['gameID',['../classGame.html#ac28e4514efb0379adc99bbf1e4e1997b',1,'Game::gameID()'],['../classGameStateService.html#ab863ee6c2cf6f575b1e7efd37ffc5130',1,'GameStateService::gameId()']]],
  ['gamephase_606',['gamePhase',['../classGame.html#a6a9fe4953f94562d5d6d2aae75935a46',1,'Game']]],
  ['gamephasetostr_607',['gamePhaseToStr',['../game_8cpp.html#a97cf14a397ed31f6f3aae282542c04c0',1,'gamePhaseToStr(GamePhase gamePhase):&#160;game.cpp'],['../game_8hpp.html#a97cf14a397ed31f6f3aae282542c04c0',1,'gamePhaseToStr(GamePhase gamePhase):&#160;game.cpp']]],
  ['gamestatechanged_608',['gameStateChanged',['../classGameStateService.html#a6f9e21cf229558b2e6bf4d1b8387e1e5',1,'GameStateService']]],
  ['gamestateservice_609',['GameStateService',['../classGameStateService.html#afe7ace63d568f4793becce8bf7878cee',1,'GameStateService']]],
  ['get_610',['GET',['../classNetworkService.html#a902ac1cc48a670802152f778b567ad0d',1,'NetworkService']]],
  ['getbody_611',['getBody',['../classRequest.html#ae773f518204df6572162d99ac2dba7db',1,'Request']]],
  ['getcardid_612',['getCardId',['../classCardWidget.html#a6b35ac3269d57685078de65479c03db0',1,'CardWidget']]],
  ['getinstance_613',['getInstance',['../classGameStateService.html#af20dc709a18caf26f309e3c322beb631',1,'GameStateService::getInstance()'],['../classSettingsService.html#a53e5dced83d37fac8dcbcb04a0446f56',1,'SettingsService::getInstance()'],['../classNetworkService.html#af73052724a162e5f4c33375f94ac4bfd',1,'NetworkService::getInstance()']]],
  ['getmethod_614',['getMethod',['../classRequestHeader.html#a64f50bc9d7cb3fb39d23f4c987e7eb57',1,'RequestHeader']]],
  ['getpath_615',['getPath',['../classRequestHeader.html#a8a423256d67ad4d3af3515bca5d06219',1,'RequestHeader']]],
  ['getsocketfd_616',['getSocketFd',['../classRequest.html#a4406da47b6d4108dbb172cec782a06de',1,'Request']]]
];
