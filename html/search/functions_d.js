var searchData=
[
  ['read_668',['read',['../classSettingsService.html#a0c952e5343c99b13c7cab66b13baf28a',1,'SettingsService']]],
  ['readcoords_669',['readCoords',['../classTable.html#a2c1bad632b4cabfdc93d32e586308641',1,'Table']]],
  ['readfromclient_670',['readFromClient',['../classMoxitServer.html#a0e3a012b7cdb32afda6ca56e017e90be',1,'MoxitServer']]],
  ['readjsonfile_671',['readJsonFile',['../classTable.html#a75a57169006631763be43d6af6c3ad12',1,'Table::readJsonFile()'],['../classSettings.html#a8d3a20e1fac2c8405625634c77f7fa66',1,'Settings::readJsonFile()']]],
  ['removecardwidget_672',['removeCardWidget',['../classTable.html#a2d303ada19507af8560638ffd763e925',1,'Table']]],
  ['removeplayer_673',['removePlayer',['../classGame.html#a5088218506361da372f8333034c79a2c',1,'Game']]],
  ['render_674',['render',['../classHostGame.html#a05a7c5668a4264ed621a72dd86ee1268',1,'HostGame::render()'],['../classTable.html#a5fc1bb37c3d25588da78dc6df9792f59',1,'Table::render()']]],
  ['request_675',['Request',['../classRequest.html#adcb4de0b9159115b103f6f6403b9af39',1,'Request']]],
  ['requestheader_676',['RequestHeader',['../classRequestHeader.html#aa755158b5725111b734f7c99136a2026',1,'RequestHeader']]],
  ['response_677',['Response',['../classResponse.html#a8201fb49ce101abdb4646c32a5b3c15e',1,'Response']]],
  ['responsecontent_678',['responseContent',['../classNetworkService.html#aead3be14e1aabce8cb4b0c33ddcff5e8',1,'NetworkService']]],
  ['responsewritecallback_679',['responseWriteCallback',['../NetworkService_8hpp.html#aca0f70d536f7bbd7a035ba91a150684a',1,'responseWriteCallback(void *data, size_t size, size_t nmemb, void *userData):&#160;NetworkService.cpp'],['../NetworkService_8cpp.html#aca0f70d536f7bbd7a035ba91a150684a',1,'responseWriteCallback(void *data, size_t size, size_t nmemb, void *userData):&#160;NetworkService.cpp']]]
];
