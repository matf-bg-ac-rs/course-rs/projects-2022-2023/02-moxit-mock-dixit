var searchData=
[
  ['scoredots_836',['scoreDots',['../classTable.html#a72e3bd615edada2744077b583be0cedf',1,'Table']]],
  ['scoresum_837',['scoreSum',['../classTable.html#ade41e8d3ae2349f36330c42541b13375',1,'Table']]],
  ['second_838',['SECOND',['../classConfig.html#a9b70a6c3ed2588aa0abeacf7a1639519',1,'Config']]],
  ['settings_839',['settings',['../classSettingsService.html#a9d2cbf4887c25dded6f87a67a3287573',1,'SettingsService::settings()'],['../classMainWindow.html#a914d53c489b015b0bee55448a76b163e',1,'MainWindow::settings()']]],
  ['settings_5ffile_840',['SETTINGS_FILE',['../classConfig.html#a688c7386349a0f4e557dd6e1bf2addea',1,'Config']]],
  ['settingsservice_841',['settingsService',['../classGameStateService.html#a57fc7f3b4087fe4e5d3c285df292a8ad',1,'GameStateService::settingsService()'],['../classHostGame.html#a4bd3d4afe2a2028499f94caba3289aa6',1,'HostGame::settingsService()'],['../classTable.html#a49418493265a2768360c55a39939faeb',1,'Table::settingsService()'],['../classSettings.html#af5a2fc639bb1b55f5913131706dcef9e',1,'Settings::settingsService()']]]
];
