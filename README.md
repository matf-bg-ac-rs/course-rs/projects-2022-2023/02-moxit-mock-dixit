## Moxit

Moxit je implementacija društvene igre Dixit u C++. Narator se smenjuje među igračima po rundama u krug. Narator bira kartu i vezuje asocijaciju u vidu reči ili rečenice za odabranu kartu. Bez otkrivanja naratorove karte, ali sa otkrivanjem njegove asocijacije, igrači svaki za sebe biraju kartu koja ga najviše asocira na datu asocijaciju. Potom sledi kolektivno glasanje gde igrači koji nisu narator pogađaju naratorovu kartu. Poeni se dodeljuju u skladu sa time da li je karta pravilno pogođena - i ako nije - čija karta je pogrešno pogođena. Igračima je cilj da što pre stignu do skupljenih 30 bodova. Prvi koji stigne do kraja je pobednik.

### Potrebne biblioteke

Za uspešno bildovanje projekta neophodne su sledeće biblioteke:

- Qt5
- Qt5Multimedia
- libcurl

Za testiranje NetworkService klase neophodna je i sledeca biblioteka, kao i linkovanje curl biblioteke sa -lcurl:

- nlohmann/json

Za linux ubuntu operativni sistem 20.04 dovoljno je pokrenuti komandu:

`sudo apt-get install qtbase5-dev qtchooser qt5-qmake qtbase5-dev-tools qtmultimedia5-dev libcurl4-gnutls-dev`

U slučaju da se prilikom pokretanja pojavljuje greška 
`defaultServiceProvider::requestService(): no service found for - "org.qt-project.qt.mediaplayer"` i muzika se ne čuje, neophodno je obezbediti Qt5Multiemedia plugine. Na Ubuntu sistemima dovoljno je sledeće:

`sudo apt-get install libqt5multimedia5-plugins`

Za testiranje NetworkService klase:

`sudo apt-get install nlohmann-json3-dev`

Demonstracija projekta se može pronaći na sledećem linku https://www.youtube.com/watch?v=YvyTze4Vmy8&feature=youtu.be .

Frontend i backend su aplikacije za sebe i svaka zahteva buildovanje zasebno. Buildovanje se vrši korišćenjem CMakeLists.txt fajlova u frontend/src i backend/src. Za rad aplikacija na lokalnom računaru neophodno je držati backend server deo pokrenut. U slučaju dizanja servera na udaljeni računar neophodno je uskladiti API_ENDPOINT promenljivu u frontend/src/lib/Main Window/config.cpp.

Minimalan broj igrača potreban za testiranje je 2, i to svako na svojoj instanci aplikacije. Međutim, potreban broj igrača za pravilno igranje igre je 3-8.

Dijagrami klasa su generisani korišćenjem Doxygen alata (html/index.html strana) 

### Slike

![](slike/1.jpg)
![](slike/4.jpg)
![](slike/5.jpg)
![](slike/2.jpg)
![](slike/3.jpg)
