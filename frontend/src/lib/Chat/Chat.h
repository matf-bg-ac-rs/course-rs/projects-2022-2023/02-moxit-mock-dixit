#ifndef CHAT_H
#define CHAT_H

#include<QStringListModel>
#include<QStandardItemModel>
#include<fmt/core.h>
#include <QWidget>

#include<QListView>
#include<QLineEdit>
#include<QFile>
#include<QFont>
#include<QJsonDocument>
#include<QTextEdit>
#include<QJsonArray>
#include<QThread>
#include<QTime>
#include<QStyledItemDelegate>
#include<QFontMetrics>
#include<QRect>
#include<QSize>

#include "../network_service/NetworkService.hpp"
#include "config.h"
#include "globals.h"
#include "gamestateservice.h"
#include "settingsservice.h"

QT_BEGIN_NAMESPACE
namespace Ui { class Chat; }
QT_END_NAMESPACE

class Chat : public QWidget
{
    Q_OBJECT

public:
    Chat(QWidget *parent = nullptr);
    ~Chat();


private slots:
    void showMessages();
    void sendMessage();

private:
    Ui::Chat *ui;

    bool m_sendingMessage;

    // chat
    QStringListModel *chatModel;

    QString m_username;

};
#endif // CHAT_H
