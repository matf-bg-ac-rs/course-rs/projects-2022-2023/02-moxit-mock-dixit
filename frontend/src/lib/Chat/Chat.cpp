#include "Chat.h"
#include "ui_Chat.h"

class MyDelegate : public QStyledItemDelegate {
 public:
  MyDelegate(QObject* parent)
    : QStyledItemDelegate(parent) {}

  QSize sizeHint(const QStyleOptionViewItem& option, const QModelIndex& index) const {
    QFontMetrics fm(option.font);
    const QAbstractItemModel* model = index.model();
    QString Text = model->data(index, Qt::DisplayRole).toString();
    QRect neededsize = fm.boundingRect( option.rect, Qt::TextWordWrap,Text );
    return QSize(option.rect.width(), neededsize.height());
  }
};

Chat::Chat(QWidget *parent)
    : QWidget(parent),
      ui(new Ui::Chat),
      m_username("Unknown")
{
    ui->setupUi(this);

    SettingsService &settingsService = SettingsService::getInstance();

    m_username = settingsService.username();

    GameStateService &gameStateService = GameStateService::getInstance();

    ui->listView->setStyleSheet("border: 4px solid #088F8F;"
                                "border-radius: 10px;"
                                "background: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,stop: 0 #FFFDD0, stop: 1 #CCCA9F);"
                                "font-size: 15px"
                                );
    QFont serifFont("Times", 10, QFont::Bold);
    ui->listView->setFont(serifFont);

    ui->lineEdit->setStyleSheet("border: 4px solid #088F8F;"
                                 "border-radius: 10px;"
                                 "padding: 0 8px;"
                                 "background: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,stop: 0 #FFFDD0, stop: 1 #CCCA9F);"
                                 "font-size: 15px"
                                );
    ui->lineEdit->setPlaceholderText("Enter your message");
    QFont italicFont("Times", 10, QFont::StyleItalic);
    ui->lineEdit->setFont(italicFont);

    ui->listView->setWordWrap(true);
    ui->listView->setItemDelegate(new MyDelegate(this));

    showMessages();

    connect(ui->lineEdit, &QLineEdit::returnPressed, this, &Chat::sendMessage);
    connect(&gameStateService, &GameStateService::gameStateChanged, this, &Chat::showMessages);

}

Chat::~Chat()
{
    delete ui;
}

void Chat::showMessages(){

    //CHAT
    chatModel = new QStringListModel(this);
    chatModel->insertColumn(0);

    ui->listView->setModel(chatModel);

    QJsonArray messages = globalGameState["chat"].toArray();

    for(int i=0; i < messages.size(); ++i) {
        const QJsonValue &message = messages[i];

        QString formatirano = QString(
                    fmt::format(R"([{}] {}: {})",
                                    message["time"].toString().toStdString(),
                                    message["userName"].toString().toStdString(),
                                    message["content"].toString().toStdString()
                                ).c_str());

        int newRow = chatModel->rowCount();
        chatModel->insertRow(newRow);
        chatModel->setData(chatModel->index(newRow,0), formatirano);
        chatModel->setData(chatModel->index(newRow, 0), int(Qt::AlignLeft | Qt::AlignVCenter), Qt::TextAlignmentRole);

    }


    ui->listView->scrollToBottom();
}

void Chat::sendMessage()
{
    QString content = ui->lineEdit->text();
    if(content.size() == 0) return;

    NetworkService &networkService = NetworkService().getInstance();

    networkService.POST(Config::API_ENDPOINT_CHAT, fmt::format(R"(
        {{
            "gameId": {},
            "userName": "{}",
            "content": "{}",
            "time": "{}"
        }}
    )",
        globalGameState["id"].toInt(),
        m_username.toStdString(),
        content.toStdString(),
        QTime::currentTime().toString().toStdString()
                        ));

    if(networkService.statusCode() != CURLE_OK) {
        std::cerr << networkService.responseContent() << std::endl;
    }
    else {
        ui->lineEdit->setText("");
    }

}
