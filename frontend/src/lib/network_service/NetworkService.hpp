#ifndef __NETWORK_SERVICE_H__
#define __NETWORK_SERVICE_H__

#include <iostream>
#include <string>
#include <cstring>
#include <ostream>
#include <unordered_map>
#include <curl/curl.h>

class NetworkService
{
    private:
        CURLcode responseCode_;
        std::string responseData_;
        char* responseErrorBuffer_;

    public:
        NetworkService();
        ~NetworkService();

        static NetworkService& getInstance();

        CURLcode GET(
            const std::string &url,
            const std::unordered_map<std::string, std::string> &params = {},
            const std::unordered_map<std::string, std::string> &headers = {}
            );

        CURLcode POST(
            const std::string &url,
            const std::string &data,
            const std::unordered_map<std::string, std::string> &headers = {}
        );
        
        friend size_t responseWriteCallback(void *data, size_t size, size_t nmemb, void *userData);

        NetworkService(NetworkService const&) = delete;
        void operator=(NetworkService const&) = delete;

        void test();

        std::string responseContent();
        CURLcode statusCode() { return responseCode_; }
    
};

size_t responseWriteCallback(void *data, size_t size, size_t nmemb, void *userData);

#endif
