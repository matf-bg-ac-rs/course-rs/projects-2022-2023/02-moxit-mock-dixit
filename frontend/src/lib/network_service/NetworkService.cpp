#include "NetworkService.hpp"

size_t responseWriteCallback(void *data, size_t size, size_t nmemb, void *userData)
{

    (void)userData;
    //std::cout << "Writing chunK!" << std::endl;

    NetworkService &networkService = NetworkService::getInstance();

    size_t chunkSize = size * nmemb;
    networkService.responseData_.append((const char*)data ,chunkSize);
    return chunkSize;
}

NetworkService::NetworkService()
{
    curl_global_init(CURL_GLOBAL_ALL);
    responseCode_ = CURLE_OK;
    responseErrorBuffer_ = nullptr;
}

NetworkService::~NetworkService()
{
    curl_global_cleanup();
}

NetworkService& NetworkService::getInstance()
{
    static NetworkService instance;
    return instance;
}

CURLcode NetworkService::GET(
    const std::string &_url,
    const std::unordered_map<std::string, std::string> &params,
    const std::unordered_map<std::string, std::string> &headers
    )
{
    CURL *curl = curl_easy_init();

    std::string url = _url;

    if(params.size() > 0) {

        url += "?dummy=dummy&";

        for(auto param : params) {
            url += '&';
            url += param.first;
            url += '=';
            url += param.second;
        }
    }

    struct curl_slist *headers_ = NULL;

    if(headers.size() > 0) {
        for(auto header: headers) {
            headers_ = curl_slist_append(headers_, (header.first +": " + header.second).c_str());
        }
    }
    
    CURLcode respCode;
    
    if(curl) {
        curl_easy_setopt(curl, CURLOPT_URL, url.c_str());
        curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, responseWriteCallback);
        curl_easy_setopt(curl, CURLOPT_WRITEDATA, (void *)nullptr);

        curl_easy_setopt(curl, CURLOPT_ERRORBUFFER, responseErrorBuffer_);

        responseData_ = "";

        respCode = curl_easy_perform(curl);
        responseCode_ = respCode;

        curl_easy_cleanup(curl);
        return respCode;
    }
    else throw std::invalid_argument("Curl pointer is null");
}

CURLcode NetworkService::POST(
    const std::string &_url,
    const std::string &data,
    const std::unordered_map<std::string, std::string> &headers
    )
{
    CURL *curl = curl_easy_init();

    std::string url = _url;

    struct curl_slist *headers_ = NULL;

    if(headers.size() > 0) {
        for(auto header: headers) {
            headers_ = curl_slist_append(headers_, (header.first +": " + header.second).c_str());
        }
    }
    
    CURLcode respCode;
    responseCode_ = CURL_LAST;
    
    if(curl) {

        const char *post_data = data.c_str();

        curl_easy_setopt(curl, CURLOPT_URL, url.c_str());
        curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, responseWriteCallback);
        curl_easy_setopt(curl, CURLOPT_WRITEDATA, (void *)nullptr);
        curl_easy_setopt(curl, CURLOPT_POSTFIELDS, post_data);

        curl_easy_setopt(curl, CURLOPT_ERRORBUFFER, responseErrorBuffer_);
        curl_easy_setopt(curl, CURLOPT_FAILONERROR, 1L);

        responseData_ = "";

        respCode = curl_easy_perform(curl);
        responseCode_ = respCode;

        curl_easy_cleanup(curl);
        return respCode;
    }
    else throw std::invalid_argument("Curl pointer is null");
}

void NetworkService::test()
{
    std::cout << "Hello world!\n";
}

std::string NetworkService::responseContent()
{
    if(responseCode_ == 0) {
        return responseData_;
    }
    else {
        return std::string("CURL ERROR: ") + std::string(curl_easy_strerror(responseCode_));
    }
}
