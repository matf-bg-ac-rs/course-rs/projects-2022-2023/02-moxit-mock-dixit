#define CATCH_CONFIG_MAIN
#include "catch.hpp"
#include "NetworkService.hpp"
#include <curl/curl.h>
#include <nlohmann/json.hpp>
#include <unordered_map>
#include <string>

using json = nlohmann::json;

TEST_CASE("NetworkService connects to a remote resource","[network_service][connectivity]")
{
    NetworkService &networkService = NetworkService().getInstance();

    const char *input = "https://reqres.in/api/users/2";
    CURLcode expected = CURLE_OK;

    networkService.GET(input);

    REQUIRE(networkService.statusCode() == expected);

}

TEST_CASE("NetworkService GET request NO Headers/Parameters","[network_service][get][no_headers][no_params]")
{
    NetworkService &networkService = NetworkService().getInstance();
    int expected = 2;

    networkService.GET("https://reqres.in/api/users/2");
    CHECK(networkService.statusCode() == CURLE_OK);

    std::string input = networkService.responseContent();

    json respJson = json::parse(networkService.responseContent());

    REQUIRE(respJson["data"]["id"] == expected);

}

TEST_CASE("NetworkService GET request WITH Parameters NO Headers","[network_service][get][params][no_headers]")
{
    NetworkService &networkService = NetworkService().getInstance();
    const char *inputApiEndpoint = "https://reqres.in/api/users";

    std::unordered_map<std::string,std::string> inputParams = {
        {"page", "5"}
    };

    int expectedOutputPage = 5;

    networkService.GET(inputApiEndpoint, inputParams);

    CHECK(networkService.statusCode() == CURLE_OK);

    json respJson = json::parse(networkService.responseContent());

    REQUIRE(respJson["page"] == expectedOutputPage);
}

TEST_CASE("NetworkService POST request NO Headers/Params","[network_service][post][no_headers][no_params]") 
{
    NetworkService &networkService = NetworkService().getInstance();

    std::string inputData = std::string(R"({
        "name": "morpheus",
        "job": "leader"
    })");

    const char *url = "https://reqres.in/api/users";

    networkService.POST(url, inputData);

    CHECK(networkService.statusCode() == CURLE_OK);

    json respData = json::parse(networkService.responseContent());

    REQUIRE(respData["id"] != nullptr);
    REQUIRE(respData["id"] != "");

}