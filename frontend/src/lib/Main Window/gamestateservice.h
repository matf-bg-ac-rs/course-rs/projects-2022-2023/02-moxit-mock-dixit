#ifndef GAMESTATESERVICE_H
#define GAMESTATESERVICE_H

#include <iostream>
#include <string>
#include <QObject>
#include <QTimer>
#include <QJsonDocument>
#include <QJsonValue>
#include <fmt/core.h>
#include <curl/curl.h>
#include "globals.h"
#include "config.h"
#include "settingsservice.h"
#include "../network_service/NetworkService.hpp"

class GameStateService : public QObject
{
    Q_OBJECT
private:
    QTimer* gameStateFetchLoop;
    QJsonDocument *gameState;
    QJsonDocument prevGameState;
    SettingsService &settingsService;

    explicit GameStateService(QObject *parent = nullptr);
public:

    bool amIHost();
    bool isPlayerHost(int id);
    int gameId();
    bool amINarrator();
    int myId();

    static GameStateService& getInstance()
    {
        static GameStateService _instance;
        return _instance;
    }

    void startGameStateFetchLoop();
    void endGameStateFetchLoop();

public slots:
    void fetchGameState();

signals:
    void gameStateChanged();

};

#endif // GAMESTATESERVICE_H
