#include "settings.h"
#include "ui_settings.h"
#include "ui_settings.h"

Settings::Settings(QWidget *parent)
    //: QWidget(parent)
    : ui(new Ui::Settings),
      settingsService(SettingsService::getInstance())
{

    ui->setupUi(this);
    QPixmap bkgnd(":/images/whale.jpg");
    bkgnd = bkgnd.scaled(this->size(), Qt::IgnoreAspectRatio);
    QPalette palette;
    palette.setBrush(QPalette::Window, bkgnd);
    this->setPalette(palette);

    ui->hsStory->setValue(settingsService.settings["dur_story"].toInt());
    ui->hsCard->setValue(settingsService.settings["dur_card"].toInt());
    ui->hsVoting->setValue(settingsService.settings["dur_voting"].toInt());
    ui->lnUsername->insert(settingsService.settings["username"].toString());
    ui->sbPlayers->setValue(settingsService.settings["num_of_players"].toInt());

}

Settings::~Settings()
{

    delete ui;
}

void Settings::on_back_clicked()
{
    close();
}



void Settings::on_hsStory_sliderMoved(int position)
{
    if(position == 60)
        dur_story = 60;
    else if(position == 90)
        dur_story = 90;
    else if(position == 120)
        dur_story = 120;

    settingsService.settings["dur_story"] = dur_story;

}




void Settings::on_hsCard_sliderMoved(int position)
{

    if(position == 60)
        dur_card = 60;
    else if(position == 90)
        dur_card = 90;
    else if(position == 120)
        dur_card = 120;

    settingsService.settings["dur_card"] = dur_card;

}


void Settings::on_hsVoting_sliderMoved(int position)
{

    if(position == 60)
        dur_voting = 60;
    else if(position == 90)
        dur_voting = 90;
    else if(position == 120)
        dur_voting = 120;

    settingsService.settings["dur_voting"] = dur_voting;

}


void Settings::on_sbPlayers_valueChanged(int arg1)
{
    num_of_players = arg1;
    settingsService.settings["num_of_players"] = num_of_players;

}

void Settings::on_lnUsername_textChanged(const QString &arg1)
{
    username = arg1;
    settingsService.settings["username"] = username;
}

void Settings::on_save_clicked()
{
    close();
}






