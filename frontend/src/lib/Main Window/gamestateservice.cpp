#include "gamestateservice.h"

GameStateService::GameStateService(QObject *parent)
    : QObject{parent},
      gameStateFetchLoop(nullptr),
      gameState(&globalGameState),
      prevGameState(globalGameState),
      settingsService(SettingsService::getInstance())
{
}

bool GameStateService::amIHost()
{
    const QJsonValue &playersValue = (*gameState)["players"];
    return playersValue["0"]["username"].toString() == settingsService.username();
}

bool GameStateService::isPlayerHost(int id)
{
    return id == (*gameState)["players"]["hostId"].toInt();
}

int GameStateService::gameId()
{
    return (*gameState)["id"].toInt();
}

bool GameStateService::amINarrator()
{
    return (*gameState)["players"][QString::number(
                (*gameState)["narratorId"].toInt()
            )]["username"].toString() == settingsService.username();
}

int GameStateService::myId()
{
    const QJsonValue &playersValue = globalGameState["players"];
    const QJsonObject &players = playersValue.toObject();

    for(auto &key : players.keys()) {
        if(players[key]["username"] == settingsService.username()) {
            return key.toInt();
        }
    }

    return -1;
}

void GameStateService::fetchGameState()
{
    if(gameState != nullptr) {
        NetworkService &networkService = NetworkService().getInstance();

        networkService.POST(Config::API_ENDPOINT_GAME_STATE, fmt::format(R"(
            {{
                "gameId": {}
            }}
        )",(*gameState)["id"].toInt()));

        if(networkService.statusCode() == CURLE_OK) {
            (*gameState) = QJsonDocument::fromJson(
                            QString(networkService.responseContent().c_str()).toUtf8()
                        );

            if((*gameState).toJson().toStdString() != prevGameState.toJson().toStdString())
            {
                emit gameStateChanged();
                //std::cerr << (*gameState).toJson().toStdString() << std::endl;
            }

            prevGameState = *gameState;
        }
    }

}

void GameStateService::startGameStateFetchLoop()
{
    endGameStateFetchLoop();
    gameStateFetchLoop = new QTimer(this);
    connect(gameStateFetchLoop, &QTimer::timeout, this, &GameStateService::fetchGameState);
    gameStateFetchLoop->start(Config::SECOND);
}

void GameStateService::endGameStateFetchLoop()
{
    if(gameStateFetchLoop != nullptr) {
        gameStateFetchLoop->stop();
    }
}
