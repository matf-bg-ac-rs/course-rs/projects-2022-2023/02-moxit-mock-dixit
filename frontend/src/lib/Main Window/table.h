#ifndef TABLE_H
#define TABLE_H

#include <QMainWindow>
#include "cardwidget.h"
#include <QJsonObject>
#include <QJsonArray>
#include <QJsonDocument>
#include <QByteArray>
#include <QFile>
#include <QJsonValue>
#include <QPixmap>
#include <QPalette>
#include <QBrush>
#include <QPushButton>
#include <QString>
#include <iostream>
#include <numeric>
#include <QDirIterator>
#include <QString>
#include <iostream>
#include <unordered_map>
#include <QPainter>
#include <QRandomGenerator>
using namespace std;

#include "config.h"
#include "globals.h"
#include "../Chat/Chat.h"
#include "../network_service/NetworkService.hpp"
#include "settingsservice.h"
#include "gamestateservice.h"


QT_BEGIN_NAMESPACE
namespace Ui { class Table; }
QT_END_NAMESPACE

class Table : public QMainWindow
{
    Q_OBJECT

public:
    Table(QWidget *parent = nullptr);
    ~Table();

    void addCard(CardWidget *cw);

    void addCardWidget(CardWidget *card);

    void removeCardWidget();

    void render();

    void readJsonFile();

    QString findImage(int i);

    void readCoords();

    void addPlayer(int id);

private slots:
    void on_leStory_textChanged(const QString &arg1);

private:
    Ui::Table *ui;
    CardWidget *card;
    int hostId;
    int maxPlayers;
    QString gamePhase;
    int currNarrator;
    int narratorCard;
    QString clue;
    int narratorId;
    int voteCounter;
    std::unordered_map<int, int> votes;
    bool narrCardChosen = false;
    int playerId;
    int cardId;
    std::unordered_map<int, vector<int>> players;
    QString id;
    std::vector<int> hand;
    int pressed = false;
    std::unordered_map<int, int> commonPile;
    unordered_map<int, int> playersId;
    unordered_map<int, QString> username;
    int noPls;
    int prevCard;
    unordered_map<int, int> playersPoints;
    static const QPoint coords[31];
    int buttonId = 0;
    unordered_map<int, QPushButton*> buttons;

    unordered_map<int, int> playerScores;
    unordered_map<int, QPushButton*> scoreDots;
    unordered_map<int, QString> playerColors;

    int scoreSum;
    bool votedForCard;
    bool choseCard;
    int chosenCard;

    GameStateService &gameStateService;
    NetworkService &networkService;
    SettingsService &settingsService;

private slots:
    void onCardClicked(int);

};
#endif // TABLE_H
