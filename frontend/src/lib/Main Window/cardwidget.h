#ifndef CARDWIDGET_H
#define CARDWIDGET_H

#include <iostream>
#include <fmt/core.h>
#include <QWidget>
#include <QPushButton>

class CardWidget : public QPushButton
{
    Q_OBJECT
public:
    explicit CardWidget(int id, QWidget *parent = nullptr);

    ~CardWidget();

    int getCardId();

private:
    int cardId;

signals:
    void onCardClicked(int id);

};

#endif // CARDWIDGET_H
