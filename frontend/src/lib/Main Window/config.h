#ifndef CONFIG_H
#define CONFIG_H

#include <string>

class Config
{
public:
    static const std::string API_ENDPOINT;
    static const std::string API_ENDPOINT_CREATE_GAME;
    static const std::string API_ENDPOINT_JOIN_GAME;
    static const std::string API_ENDPOINT_GAME_STATE;
    static const std::string API_ENDPOINT_CHAT;
    static const std::string API_ENDPOINT_START_GAME;
    static const std::string API_ENDPOINT_ACTIVE_GAMES;
    static const std::string API_ENDPOINT_NARRATOR_CHOICE;
    static const std::string API_ENDPOINT_PLAYER_CHOICE;
    static const std::string API_ENDPOINT_PLAYER_VOTE;
    static const char * SETTINGS_FILE;
    static constexpr int SECOND = 1000;
    static constexpr int HTTP_OK = 200;
private:
};

#endif // CONFIG_H
