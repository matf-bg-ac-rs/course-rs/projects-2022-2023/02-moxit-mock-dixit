#include "settingsservice.h"

SettingsService::SettingsService(QObject *parent)
    : QObject{parent}
{
    read();
}

SettingsService::~SettingsService()
{
    write();
}

void SettingsService::read()
{
    QFile settingsFile = QFile(Config::SETTINGS_FILE);

    if(!settingsFile.open(QIODevice::ReadOnly)) {
       std::cerr << "Error opening settings file " << Config::SETTINGS_FILE << std::endl;

       std::ofstream settingsFile = std::ofstream(Config::SETTINGS_FILE);
       settingsFile << fmt::format(R"(
            {{
                "username": "{}",
                "num_of_players": 5
            }}
        )", std::string("Player_") + std::to_string(time(NULL)));

       settingsFile.close();

    }


    settings = QJsonDocument::fromJson(
                settingsFile.readAll()
                ).object();


}

void SettingsService::write()
{
    std::ofstream outputJson(Config::SETTINGS_FILE);
    outputJson << QJsonDocument(settings).toJson().toStdString();
}

const QString SettingsService::username() const
{
    return settings["username"].toString();
}

int SettingsService::numberOfPlayers() const
{
    return settings["num_of_players"].toInt();
}
