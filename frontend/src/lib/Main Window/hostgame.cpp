#include "hostgame.h"
#include "ui_hostgame.h"

HostGame::HostGame(QWidget *parent)
    //: QDialog(parent)
    : ui(new Ui::HostGame),
      m_startGameBtnConnected(false),
      gameStateService(GameStateService::getInstance()),
      settingsService(SettingsService::getInstance())
{

    ui->setupUi(this);

    this->setWindowTitle(this->windowTitle() + " - " + settingsService.username());

    this->setAttribute(Qt::WA_DeleteOnClose);

    QPixmap bkgnd(":/images/lobby.jpg");
    bkgnd = bkgnd.scaled(this->size(), Qt::IgnoreAspectRatio);
    QPalette palette;
    palette.setBrush(QPalette::Window, bkgnd);
    this->setPalette(palette);

    ui->gameCode->setText(QString("Game ID: ") + QString::number(globalGameState["id"].toInt()));
    ui->gameCode->setTextInteractionFlags(Qt::TextSelectableByMouse);

    ui->chatVerticalLayout->addWidget(new Chat(this));
    connect(&gameStateService, &GameStateService::gameStateChanged, this, &HostGame::render);

    render();

}

HostGame::~HostGame()
{
    delete ui;
}


void HostGame::on_pbBack_clicked()
{
    gameStateService.endGameStateFetchLoop();
    close();
}

void HostGame::on_pushButton_clicked()
{

}

void HostGame::render()
{
    ui->lblPlayers->setText("");

    QString lblPlayersText = "";

    const QJsonValue &playersValue = globalGameState["players"];
    const QJsonObject &players = playersValue.toObject();

    for(auto &key : players.keys()) {
        const QJsonValue &player = players[key];
        lblPlayersText.append(player["username"].toString());

        if(gameStateService.isPlayerHost(player["id"].toInt())) {
            lblPlayersText.append(QString(" (Host)"));
        }

        lblPlayersText.append("\n\n");
    }

    ui->lblPlayers->setText(lblPlayersText);

    int maxPlayers = globalGameState["maxPlayers"].toInt();

    // Set remaining players
    ui->players->setText(
                QString(fmt::format(R"(Players ({}/{}):)", players.size(), maxPlayers).c_str())
                );

    // Start game coloration
    static QRegularExpression bckg = QRegularExpression("background-color: *[^;]+;?");
    QString btnStartGameStyleSheet = ui->btnStartGame->styleSheet();

    QString replaceWith;

    if(players.size() == maxPlayers && gameStateService.amIHost() && !globalGameState["isGameStarted"].toBool()) {
        replaceWith = QString("background-color: green;");

        // Start game should be clickable
        if(!m_startGameBtnConnected) {
            connect(ui->btnStartGame, &QPushButton::clicked, this, [this]() {
                NetworkService &networkService = NetworkService::getInstance();

                std::string data = fmt::format(R"(
                    {{
                        "gameId": {}
                    }}
                )", globalGameState["id"].toInt());

                networkService.POST(Config::API_ENDPOINT_START_GAME, data);

                if(networkService.statusCode() != CURLE_OK) {
                    std::cerr << networkService.responseContent() << std::endl;
                }
            });

            m_startGameBtnConnected = true;
        }
    }
    else {
        replaceWith = QString("background-color: lightgray;");
    }

    auto matches = bckg.globalMatch(btnStartGameStyleSheet);
    QRegularExpressionMatch firstMatch = matches.next();

    ui->btnStartGame->setStyleSheet(
                    btnStartGameStyleSheet.replace(
                        firstMatch.capturedStart(),
                        firstMatch.capturedLength(),
                        replaceWith
                    )
                );


    // Game is started, move to table
    if(globalGameState["isGameStarted"].toBool() == true) {
        Table *gameTable = new Table(parentWidget());
        gameTable->show();
        this->close();
    }

}

