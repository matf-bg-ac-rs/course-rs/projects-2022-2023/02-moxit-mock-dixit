#ifndef LOBBY_H
#define LOBBY_H

#include <QMainWindow>

QT_BEGIN_NAMESPACE
namespace Ui { class Lobby; }
QT_END_NAMESPACE

#include <QJsonDocument>
#include <QFile>
#include <fmt/core.h>
#include <curl/curl.h>

#include "config.h"
#include "globals.h"
#include "settingsservice.h"
#include "gamestateservice.h"
#include "hostgame.h"
#include "../network_service/NetworkService.hpp"

class MainWindow;

class Lobby : public QMainWindow
{
    Q_OBJECT

public:
    Lobby(QWidget *parent = nullptr);
    ~Lobby();

private slots:
    void on_pbBack_clicked();

    void on_pbJoin_pressed();

    void on_pbJoin_clicked();

private:
    NetworkService &networkService;
    Ui::Lobby *ui;
    MainWindow *mainWindow;
    void loadActiveGames();
};
#endif // LOBBY_H

