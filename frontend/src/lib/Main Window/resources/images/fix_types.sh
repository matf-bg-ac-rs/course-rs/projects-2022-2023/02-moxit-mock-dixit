#!/bin/sh

for img in $(ls | grep -Po '^\d{1,2}\.\w+'); do
        ext=$(echo "$img" | grep -Po '(?<=\.)\w+')
        img_name=$(echo "$img" | grep -Po '\w+(?=\.)')
        [ $ext != "jpg" ] && convert $img "${img_name}.jpg" && rm $img
        #[ $ext != "jpg" ] && echo $img
done
