#include "table.h"
#include "./ui_table.h"

const QPoint Table::coords[31] = {
    QPoint(1170, 110),
    QPoint(1060, 120),
    QPoint(990, 110 ),
    QPoint(910, 120 ),
    QPoint(840, 140 ),
    QPoint(770, 100 ),
    QPoint(720, 130 ),
    QPoint(640, 110 ),
    QPoint(580, 130 ),
    QPoint(510, 120 ),
    QPoint(420, 130 ),
    QPoint(330, 120 ),
    QPoint(250, 130 ),
    QPoint(160, 140 ),
    QPoint(90, 110  ),
    QPoint(90, 60   ),
    QPoint(170, 50  ),
    QPoint(270, 20  ),
    QPoint(360, 30  ),
    QPoint(420, 60  ),
    QPoint(480, 60  ),
    QPoint(530, 30  ),
    QPoint(610, 40  ),
    QPoint(690, 20  ),
    QPoint(780, 20  ),
    QPoint(840, 20  ),
    QPoint(890, 50  ),
    QPoint(960, 40  ),
    QPoint(1030, 40 ),
    QPoint(1110, 40 ),
    QPoint(1180, 30 )
        };

Table::Table(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::Table),
      gameStateService(GameStateService::getInstance()),
      networkService(NetworkService::getInstance()),
      settingsService(SettingsService::getInstance()),
      scoreSum(-1),
      votedForCard(false),
      choseCard(false),
      chosenCard(-1)
{
    ui->setupUi(this);

    this->setWindowTitle(this->windowTitle() + " - " + settingsService.username());

    this->setAttribute(Qt::WA_DeleteOnClose);

    QPixmap bkgnd(":/images/resources/images/grass2.jpg");
    bkgnd = bkgnd.scaled(this->size(), Qt::IgnoreAspectRatio);
    QPalette palette;
    palette.setBrush(QPalette::Window, bkgnd);
    this->setPalette(palette);

    QPixmap pix(":/images/resources/images/cntBoard2.png");
    ui->cntBoard->setPixmap( pix.scaled( ui->cntBoard->size(), Qt::KeepAspectRatio, Qt::SmoothTransformation) );

    QPixmap pix1(":/images/resources/images/card_back.png");
    ui->deck->setPixmap( pix1.scaled( ui->deck->size(), Qt::KeepAspectRatio, Qt::SmoothTransformation) );

    ui->verticalLayoutChat->addWidget(new Chat(this));

    ui->leStory->setVisible(false);

    readJsonFile();

    // Create score dots
    float r = 15;
    QPoint pt = QPoint(r, 0);
    float cos_a = r;
    float sin_a = 0;
    const float factor = 0.71;
    for(auto &it : playerScores) {
        int _id = it.first;
        int _seed = (_id+1) * (_id+1) * 10000;
        srand(_seed);
        QPushButton *button = new QPushButton(this);

        QSize buttonSize = QSize(15,15);
        button->setFixedSize(buttonSize);

        button->move(coords[0] + pt);
        pt = QPoint(r*factor*(cos_a - sin_a), r*factor*(sin_a+cos_a));
        cos_a = pt.x()/(r*factor);
        sin_a = pt.y()/(r*factor);

        QString bckgColor = QString(fmt::format(R"(rgb({},{},{}))", rand()%255, rand()%255, rand()%255).c_str());

        button->setStyleSheet("background-color: " + bckgColor + ";");
        button->setDisabled(true);

        scoreDots[it.first] = button;
        playerColors[it.first] = bckgColor;

        button->setToolTip(username[it.first]);
        button->setMouseTracking(true);
    }

    srand(time(NULL));

    render();
    connect(&gameStateService, &GameStateService::gameStateChanged, this, &Table::render);

}


Table::~Table()
{
    gameStateService.endGameStateFetchLoop();
    delete ui;
}

void Table::render(){
    readJsonFile();

    SettingsService &settingsService = SettingsService::getInstance();
    QString userName = settingsService.username();

    // Move score dots
    float r = 15;
    QPoint pt = QPoint(r, 0);
    float cos_a = 1;
    float sin_a = 0;
    const float factor = 0.71;
    for(auto &it : playerScores) {
        int id = it.first;
        int playerScore = playerScores[id];

        scoreDots[id]->move(coords[playerScore] + pt);
        pt = QPoint(r*factor*(cos_a - sin_a), r*factor*(sin_a+cos_a));
        cos_a = pt.x()/(r*factor);
        sin_a = pt.y()/(r*factor);
    }

    // Set your hand/common pile border
    QString borderColor = playerColors[gameStateService.myId()];

    static const QString lblPileStyleSheet = ui->lblPile->styleSheet();
    QString styleSheet = lblPileStyleSheet.left(lblPileStyleSheet.count() - 2);

    QString borderColorStyleSheetRule = styleSheet;
    borderColorStyleSheetRule += "\n\tborder: 4px solid " + borderColor + ";";
    borderColorStyleSheetRule += "border-radius: 10px;";
    borderColorStyleSheetRule += "\n}";
    ui->lblPile->setStyleSheet(borderColorStyleSheetRule);

    // clear card layout
    QLayoutItem* item = nullptr;
    while((item = ui->horizontalLayout->takeAt(0))) {
        if(item) delete item->widget();
    }

    ui->lblNarratorStory->setText(clue);
    ui->lblNarratorStory->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Maximum);
    ui->lblNarratorStory->setWordWrap(true);


    ui->leStory->setVisible(false);

    if(prevCard != -1) {
            auto _item = ui->narCard->takeAt(0);
            if(_item) delete _item->widget();
            ui->narCard->addWidget(new CardWidget(prevCard));
        }

    if(gamePhase == "NARRATOR_CHOICE" || gamePhase == "PLAYERS_CHOOSE"){
        hand = players[gameStateService.myId()];

        ui->lblPile->setText("Your hand:");

        for(int cardId : hand) {
            addCardWidget(new CardWidget(cardId));
        }

        if(gameStateService.amINarrator()) {
            ui->leStory->setVisible(true);

            if(gamePhase == "PLAYERS_CHOOSE") {
                ui->leStory->setEnabled(false);
            }
            else {
                ui->leStory->setEnabled(true);
            }
        }

    }
    else{

        ui->lblPile->setText("Common pile:");

        for(QString &cardIdString : globalGameState["commonPile"].toObject().keys()) {
            addCardWidget(new CardWidget(cardIdString.toInt()));
        }

    }

}

void Table:: readJsonFile(){

    QJsonObject jsonObj = globalGameState.object();

    hostId = (jsonObj.value("hostId")).toInt();
    maxPlayers = (jsonObj.value("maxPlayers")).toInt();
    currNarrator = (jsonObj.value("currNarrator")).toInt();
    narratorCard = (jsonObj.value("narratorCard")).toInt();
    gamePhase = (jsonObj.value("gamePhase")).toString();
    clue = (jsonObj.value("clue")).toString();
    narratorId = (jsonObj.value("narratorId")).toInt();
    voteCounter = (jsonObj.value("voteCounter")).toInt();
    prevCard = (jsonObj.value("prevCard")).toInt();

    QJsonObject serializedCommonPile = (jsonObj.value("commonPile")).toObject();
    for(auto it = serializedCommonPile.begin(); it != serializedCommonPile.end(); ++it){
        int cardId = it.key().toInt();
        int playerId = it.value().toInt();
        commonPile[playerId] = cardId;
    }

    QJsonObject serializedVotes = (jsonObj.value("votes")).toObject();
    for(auto it = serializedVotes.begin(); it != serializedVotes.end(); ++it){
        int playerId = it.key().toInt();
        int cardId = it.value().toInt();
        votes[playerId] = cardId;
        cout<<votes[playerId]<<endl;
    }

    QJsonObject sPlayers = jsonObj["players"].toObject();
    cout<<sPlayers.size()<<endl;
    int k = 0;
    foreach(const QString& key, sPlayers.keys()){
        QJsonObject subObj = (sPlayers.value(key)).toObject();
        QString playerUsername = (subObj.value("username")).toString();
        int id = (subObj.value("id")).toInt();

        username[k] = playerUsername;
        playersId[k] = id;
        //playerScores[id] = subObj["score"].toInt();
        playerScores[id] = subObj["points"].toInt();

        std::vector<int> hand;
        QJsonArray sHand = (subObj.value("hand")).toArray();
        for(auto el : sHand){
            hand.push_back(el.toInt());
        }
        players[k] = hand;
        k++;
    }

}

void Table::addCardWidget(CardWidget *card) {
    ui->horizontalLayout->insertWidget(0, card);
    connect(card, &CardWidget::onCardClicked, this, &Table::onCardClicked);
}

void Table::on_leStory_textChanged(const QString &arg1)
{
    clue = arg1;
}

void Table::onCardClicked(int cardId)
{
    int _scoreSum = std::accumulate(playerScores.begin(), playerScores.end(), 0,
                                   [] (auto prev, auto &entry) {
                                        return prev + entry.second;
                                    }
                                    );

    if(_scoreSum != scoreSum) { // new Round
        scoreSum = _scoreSum;
        choseCard = false;
        votedForCard = false;
    }

    if(gamePhase == "NARRATOR_CHOICE" && gameStateService.amINarrator() && !choseCard) {

        if(ui->leStory->text().size() == 0) return;
        choseCard = true;

        QString narratorText = ui->leStory->text();

        networkService.POST(Config::API_ENDPOINT_NARRATOR_CHOICE, fmt::format(R"(
            {{
                "gameId": {},
                "cardId": {},
                "clue": "{}"
            }}
        )",
              gameStateService.gameId(),
              cardId,
              narratorText.toStdString()
        ));

        if(networkService.statusCode() != CURLE_OK) {
            std::cerr << "Narrator choice failed: " + networkService.responseContent() << std::endl;
            choseCard = false;
        }

    }
    else if(gamePhase == "PLAYERS_CHOOSE" && !gameStateService.amINarrator() && !choseCard) {

        choseCard = true;

        networkService.POST(Config::API_ENDPOINT_PLAYER_CHOICE, fmt::format(R"(
            {{
                "gameId": {},
                "cardId": {},
                "playerId": {}

            }}
        )",
            gameStateService.gameId(),
            cardId,
            gameStateService.myId()
        ));

        if(networkService.statusCode() != CURLE_OK) {
            std::cerr << "Player choice failed: " + networkService.responseContent() << std::endl;
            choseCard = false;
        }
        else {
            chosenCard = cardId;
        }
    }
    else if(gamePhase == "PLAYERS_VOTE" && !gameStateService.amINarrator() && !votedForCard){

        // Block voting for my card
        if(cardId == chosenCard) return;

        votedForCard = true;

        networkService.POST(Config::API_ENDPOINT_PLAYER_VOTE, fmt::format(R"(
            {{
                "gameId": {},
                "cardId": {},
                "playerId": {}

            }}
        )",
            gameStateService.gameId(),
            cardId,
            gameStateService.myId()
        ));

        if(networkService.statusCode() != CURLE_OK) {
            std::cerr << "Player vote failed: " + networkService.responseContent() << std::endl;
            votedForCard = false;
        }
    }
}

