#ifndef HOSTGAME_H
#define HOSTGAME_H

#include <QMainWindow>
#include <QPixmap>
#include <QPalette>
#include <QBrush>
#include <QStandardItemModel>
#include <QRegularExpression>
#include <iostream>
#include <fmt/core.h>
#include "globals.h"
#include "../Chat/Chat.h"
#include "gamestateservice.h"
#include "settingsservice.h"
#include "table.h"

QT_BEGIN_NAMESPACE
namespace Ui { class HostGame; }
QT_END_NAMESPACE

class MainWindow;

class HostGame : public QMainWindow
{
    Q_OBJECT

public:
    HostGame(QWidget *parent = nullptr);
    ~HostGame();

private:
    GameStateService &gameStateService;
    SettingsService &settingsService;
    bool m_startGameBtnConnected;

private slots:
    void on_pbBack_clicked();
    void on_pushButton_clicked();
    void render();

private:
    Ui::HostGame *ui;
    MainWindow *mainWindow;
};
#endif // HOSTGAME_H
