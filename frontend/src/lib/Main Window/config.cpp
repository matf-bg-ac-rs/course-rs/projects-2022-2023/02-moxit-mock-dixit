#include "config.h"

const std::string Config::API_ENDPOINT = "localhost:9090";
const std::string Config::API_ENDPOINT_CREATE_GAME = Config::API_ENDPOINT + "/createGame";
const std::string Config::API_ENDPOINT_JOIN_GAME = Config::API_ENDPOINT + "/joinGame";
const std::string Config::API_ENDPOINT_GAME_STATE = Config::API_ENDPOINT + "/gameState";
const std::string Config::API_ENDPOINT_CHAT = Config::API_ENDPOINT + "/chat";
const std::string Config::API_ENDPOINT_START_GAME = Config::API_ENDPOINT + "/startGame";
const std::string Config::API_ENDPOINT_ACTIVE_GAMES = Config::API_ENDPOINT + "/activeGames";
const std::string Config::API_ENDPOINT_NARRATOR_CHOICE = Config::API_ENDPOINT + "/narratorChoice";
const std::string Config::API_ENDPOINT_PLAYER_CHOICE = Config::API_ENDPOINT + "/playerChoice";
const std::string Config::API_ENDPOINT_PLAYER_VOTE = Config::API_ENDPOINT + "/playerVote";
const char* Config::SETTINGS_FILE = "settings.json";
