#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "settings.h"
#include "lobby.h"
#include "hostgame.h"
#include <QPixmap>
#include <QPalette>
#include <QBrush>
#include <QJsonDocument>
#include <QMediaPlayer>
#include <QUrl>
#include <fstream>
#include <ctime>
#include <cstdlib>
#include <fmt/core.h>
#include "../network_service/NetworkService.hpp"
#include "config.h"
#include "globals.h"
#include "gamestateservice.h"
#include "settingsservice.h"


QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void on_options_clicked();

    void on_joinGame_clicked();

    void on_exit_clicked();

    void on_newGame_clicked();

//    void resizeEvent(QResizeEvent *evt) override;

private:
    Ui::MainWindow *ui;
    Settings *settings;
    Lobby *lobby;
    HostGame *host;
};
#endif // MAINWINDOW_H
