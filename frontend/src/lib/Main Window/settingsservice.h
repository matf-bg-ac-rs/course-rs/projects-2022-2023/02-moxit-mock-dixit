#ifndef SETTINGSSERVICE_H
#define SETTINGSSERVICE_H

#include <iostream>
#include <fstream>
#include <QObject>
#include <QFile>
#include <QJsonDocument>
#include <QJsonObject>
#include <fmt/core.h>
#include <fstream>
#include "config.h"

class SettingsService : public QObject
{
    Q_OBJECT

    explicit SettingsService(QObject *parent = nullptr);
public:

    ~SettingsService();

    QJsonObject settings;

    void read();
    void write();

    const QString username() const;
    int numberOfPlayers() const;

    static SettingsService& getInstance()
    {
        static SettingsService _instance;
        return _instance;
    }

signals:

};

#endif // SETTINGSSERVICE_H
