#ifndef SETTINGS_H
#define SETTINGS_H

#include <QMainWindow>
#include<QJsonArray>
#include<QJsonObject>
#include <QMainWindow>
#include <QPixmap>
#include <QPalette>
#include <QBrush>
#include <iostream>
#include <QJsonObject>
#include <QJsonArray>
#include <QJsonDocument>
#include <QByteArray>
#include <QFile>
#include <QJsonValue>

#include "config.h"
#include "settingsservice.h"


QT_BEGIN_NAMESPACE
namespace Ui { class Settings; }
QT_END_NAMESPACE

class MainWindow;

class Settings : public QMainWindow
{
    Q_OBJECT

public:
    explicit Settings(QWidget *parent = nullptr);
    ~Settings();
    int dur_story = 30;
    int dur_card = 30;
    int dur_voting = 30;
    int num_of_players = 3;
    QString username;
    void  writeJsonFile();
    void  readJsonFile();
    SettingsService &settingsService;


private slots:
    void on_back_clicked();


    void on_hsCard_sliderMoved(int position);

    void on_hsVoting_sliderMoved(int position);

    void on_sbPlayers_valueChanged(int arg1);

    void on_hsStory_sliderMoved(int position);

    void on_save_clicked();


    void on_lnUsername_textChanged(const QString &arg1);

private:
    Ui::Settings *ui;
    MainWindow *mainWindow;
};


#endif // SETTINGS_H
