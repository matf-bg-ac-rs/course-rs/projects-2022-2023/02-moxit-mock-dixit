#include "lobby.h"
#include "ui_lobby.h"
#include <QPixmap>
#include <QPalette>
#include <QBrush>

Lobby::Lobby(QWidget *parent)
    //: QDialog(parent)
    : ui(new Ui::Lobby),
      networkService(NetworkService::getInstance())
{
    ui->setupUi(this);

    this->setAttribute(Qt::WA_DeleteOnClose);

    QPixmap bkgnd(":/images/girlFox.jpg");
    bkgnd = bkgnd.scaled(this->size(), Qt::IgnoreAspectRatio);
    QPalette palette;
    palette.setBrush(QPalette::Window, bkgnd);
    this->setPalette(palette);

    static QRegularExpression gameIdRx = QRegularExpression("[0-9]{6,}");

    connect(ui->listViewActiveGames, &QListView::doubleClicked, this, [this](const QModelIndex &idx) {
        QString rowData = idx.data().toString();
        auto match = gameIdRx.match(rowData);
        ui->leEnter->setText(match.captured());
        on_pbJoin_clicked();
    });

    ui->listViewActiveGames->hide();
    //loadActiveGames();
}

Lobby::~Lobby()
{
    delete ui;
}


void Lobby::on_pbBack_clicked()
{
    close();
}



void Lobby::on_pbJoin_pressed()
{

}


void Lobby::on_pbJoin_clicked()
{
    int gameId = ui->leEnter->text().toInt();

    if(gameId == 0) return;

    SettingsService &settingsService = SettingsService::getInstance();
    GameStateService &gameStateService = GameStateService::getInstance();

    networkService.POST(Config::API_ENDPOINT_JOIN_GAME, fmt::format(R"(
        {{
            "gameId": {},
            "userName": "{}"
        }}
    )", gameId, settingsService.username().toStdString()));

    if(networkService.statusCode() == CURLE_OK) {

       gameStateService.endGameStateFetchLoop();
       globalGameState = QJsonDocument::fromJson(QString(
                                          networkService.responseContent().c_str()
                                                        ).toUtf8());
       gameStateService.startGameStateFetchLoop();

       HostGame *hostgame = new HostGame(this);
       hostgame->show();
       this->hide();
    }
    else {
        std::cerr << networkService.responseContent() << std::endl;
    }
}

void Lobby::loadActiveGames()
{
    QStringListModel* listModel = new QStringListModel(this);
    ui->listViewActiveGames->setModel(listModel);

    networkService.POST(Config::API_ENDPOINT_ACTIVE_GAMES, "");

    if(networkService.statusCode() == CURLE_OK) {
        QJsonDocument respDoc = QJsonDocument::fromJson(
                        QString(networkService.responseContent().c_str()).toUtf8()
                    );

        QJsonArray activeGames = respDoc.array();

        for(int i=0;i<activeGames.size();++i) {
            const QJsonValue &game = activeGames[i];

            int newRow = listModel->rowCount();
            listModel->insertRow(newRow);

            listModel->setData(listModel->index(newRow,0), QString(
                                   fmt::format(R"(({}/{}) {})",
                                                game["numOfPlayers"].toInt(),
                                                game["maxPlayers"].toInt(),
                                                game["gameId"].toString().toInt()
                                               ).c_str()
                                   ));

        }

    }
    else {
        std::cerr << networkService.responseContent() << std::endl;
    }


}

