#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "Moxit_autogen/include/ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow),
      host(nullptr)
{
    ui->setupUi(this);
    QPixmap bkgnd(":/images/osnova.png");
    bkgnd = bkgnd.scaled(this->size(), Qt::IgnoreAspectRatio);
    QPalette palette;
    palette.setBrush(QPalette::Window, bkgnd);
    this->setPalette(palette);

    QMediaPlayer* player = new QMediaPlayer();
    player->setMedia(QUrl("qrc:/sounds/music.mp3"));
    player->setVolume(15);
    player->play();

}

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::on_options_clicked()
{
    Settings *settings = new Settings(this);
    settings->show();
    //hide();
}


void MainWindow::on_joinGame_clicked()
{
    Lobby *lobby = new Lobby(this);
    lobby->show();
    //hide();
}


void MainWindow::on_exit_clicked()
{
    qApp->exit();
}


void MainWindow::on_newGame_clicked()
{
    srand(time(NULL));

    NetworkService &networkService = NetworkService().getInstance();
    SettingsService &settingsService = SettingsService::getInstance();

    std::string configString = fmt::format(R"(
                                           {{
                                                "userName": "{}",
                                                "numOfPlayers": {}
                                           }}

    )",
        settingsService.username().toStdString(),
        settingsService.numberOfPlayers()
         );

    networkService.POST(
                Config::API_ENDPOINT_CREATE_GAME, configString
                );

    if(networkService.statusCode() == CURLE_OK) {

        GameStateService &gameStateService = GameStateService::getInstance();
        gameStateService.endGameStateFetchLoop();

        globalGameState = QJsonDocument::fromJson(QString(
                        networkService.responseContent().c_str()
                                                      ).toUtf8());

        gameStateService.startGameStateFetchLoop();

        host = new HostGame(this);
        host->show();

    }
    else {
        std::cerr << networkService.responseContent() << std::endl;
    }
}

