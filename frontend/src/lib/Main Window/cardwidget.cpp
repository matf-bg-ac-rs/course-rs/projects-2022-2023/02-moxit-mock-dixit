#include "cardwidget.h"
using namespace std;

CardWidget::CardWidget(int id, QWidget *parent)
    : QPushButton{parent}
{
    this->cardId = id;

    QSize BUTTON_SIZE = QSize(125, 200);
    this->setFixedSize(BUTTON_SIZE);

    QPixmap pixmap(fmt::format(R"(:/images/resources/images/{}.jpg)", cardId).c_str());
    QIcon ButtonIcon(pixmap.scaled(this->size(), Qt::KeepAspectRatio, Qt::SmoothTransformation));
    this->setIcon(ButtonIcon);
    this->setIconSize(QSize(125, 200));

    connect(this, &QPushButton::clicked, this, [this] () {
        emit this->onCardClicked(cardId);
    });

}

CardWidget::~CardWidget() {}


int CardWidget::getCardId(){
    return this->cardId;
}
