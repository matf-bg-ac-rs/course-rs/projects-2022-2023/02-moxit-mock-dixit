cmake_minimum_required(VERSION 3.5)
project(moxit-backend)
find_package(Qt5 REQUIRED COMPONENTS Core)


enable_testing()

set(CMAKE_CXX_FLAGS_DEBUG "-g")

add_subdirectory(lib/server)
add_subdirectory(lib/game)

add_executable(
    moxit-backend
    main.cpp
)

add_executable(
    run-game-test
    tests/test_game_player.cpp
)

add_test(
    NAME run-game-test
    COMMAND run-game-test
)

add_executable(
    run-chat-test
    tests/test_chat.cpp
)

add_test(
    NAME run-chat-test
    COMMAND run-chat-test
)


target_compile_options(moxit-backend PRIVATE -std=c++17 -Wall -Wextra)
target_link_libraries(moxit-backend server)
target_link_libraries(run-game-test run_game_test Qt5::Core)
target_link_libraries(run-chat-test run_game_test Qt5::Core)




