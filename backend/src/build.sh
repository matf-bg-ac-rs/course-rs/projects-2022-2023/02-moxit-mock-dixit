#!/bin/sh

[ -z "$1" ] && exit -1

sudo conan install -c tools.system.package_manager:mode=install "$1"