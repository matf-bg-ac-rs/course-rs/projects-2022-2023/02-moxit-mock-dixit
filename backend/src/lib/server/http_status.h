#ifndef HTTP_STATUS_H
#define HTTP_STATUS_H

#include <iostream>
#include <string>

using byte = unsigned char;
constexpr size_t MAX_BUFFER_SIZE = 10000;

enum class REQUEST_STATUS {
    Reading,
    Writing,
    Ended
};

struct HttpStatus {
    int m_client_fd;
    size_t m_read_header_bytes;
    size_t m_read_body_bytes;
    size_t m_sent_bytes;
    size_t m_left_to_send;
    REQUEST_STATUS m_req_status;
    std::string m_buffer;

public:
    HttpStatus(REQUEST_STATUS req_status, int client_fd = -1);
};
#endif // HTTP_STATUS_H
