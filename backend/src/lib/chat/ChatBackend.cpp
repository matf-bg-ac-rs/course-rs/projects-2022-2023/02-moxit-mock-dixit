#include "ChatBackend.hpp"
#include<iostream>

ChatBackend::ChatBackend(QJsonArray mess) : message(mess){

}

QJsonArray ChatBackend::convertToJson() const{
    return message;
}

void ChatBackend::addMessage(QJsonObject mess){
    message.push_back(mess);
}                                   
