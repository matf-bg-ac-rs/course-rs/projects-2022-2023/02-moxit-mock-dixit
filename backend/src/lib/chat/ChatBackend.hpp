#ifndef CHATBACKEND_HPP
#define CHATBACKEND_HPP

#include<iostream>
#include<QJsonObject>
#include<QJsonArray>
#include <QVector>

class ChatBackend
{
public:
    ChatBackend(QJsonArray mess);
    QJsonArray convertToJson() const;
    void addMessage(QJsonObject mess);
                                   
private:
  QJsonArray message;
};

#endif // CHATBACKEND_HPP
