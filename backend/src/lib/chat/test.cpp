#define CATCH_CONFIG_MAIN
#include "catch.hpp"

#include "ChatBackend.hpp"

TEST_CASE("Kreiranje objekta klase ChatBackend","[ChatBackend]"){

    SECTION("Konstruktor ChatBackend uspesno kreira objekat klase ChatBackend"){
        //Arrange
        QJsonArray mess;

        QJsonObject mess1;
        mess1["username"] = "Ana";
        mess1["content"] = "Cao";
        mess1["time"] = QTime::currentTime().toString();
        mess.push_back(mess1);

        QJsonObject mess2;
        mess2["username"] = "JAna";
        mess2["content"] = "hao";
        mess2["time"] = QTime::currentTime().toString();
        mess.push_back(mess2);

        //Act + Assert
        REQUIRE_NOTHROW(ChatBackend(mess));
    }
}

TEST_CASE("Dodavanje poruke u tekuci QJsonArray niz poruka", "[addMessage]"){
    SECTION("Ako je tekuci QJsonArray niz poruka prazan, funkcija uspesno dodaje novu poruku i duzina QJsonArray niza postaje 1"){

        //Arrange
        QJsonArray mess;
        ChatBackend cb(mess);

        //nova poruka
        QJsonObject mess1;
        mess1["username"] = "Ana";
        mess1["content"] = "Cao";
        mess1["time"] = QTime::currentTime().toString();

        //ocekivan izlaz
        int ocekivanIzlaz = 1;
        //Act
        //dobijeni izlaz
        cb.addMessage(mess1);
        int dobijeniIzlaz = cb.convertToJson().size();

        //Assert
        REQUIRE(ocekivanIzlaz == dobijeniIzlaz);
    }

    SECTION("Ako tekuci QJsonArray niz sadrzi 1 ili vise poruke, funkcija uspesno dodaje novu poruku i duzina QJsonArray niza postaje za 1 veca nego sto je bila"){

        //Arrange
        QJsonArray mess;
        QJsonObject mess1;
        mess1["username"] = "Ana";
        mess1["content"] = "Cao";
        mess1["time"] = QTime::currentTime().toString();
        mess.push_back(mess1);

        ChatBackend cb(mess);
        int ocekivanIzlaz = cb.convertToJson().size() + 1;
        //nova poruka
        QJsonObject mess2;
        mess2["username"] = "Pera";
        mess2["content"] = "Hello";
        mess2["time"] = QTime::currentTime().toString();

        cb.addMessage(mess2);

        //Act
        //dobijeni izlaz
        int dobijeniIzlaz = cb.convertToJson().size();

        //Assert
        REQUIRE(ocekivanIzlaz == dobijeniIzlaz);
    }
}

TEST_CASE("Vracanje tekuceg QJsonArray niza poruka", "[convertToJson]"){
    SECTION("Funkcija uspesno vraca tekuci QJsonArray niz poruka koji je prazan"){
        QJsonArray mess;

        ChatBackend cb(mess);

        QJsonArray izlaz = cb.convertToJson();
        REQUIRE(mess == izlaz);
    }

    SECTION("Funkcija uspesno vraca tekuci QJsonArray niz poruka koji sadrzi 1 i vise poruka"){
        QJsonArray mess;
        QJsonObject mess1;
        mess1["username"] = "Pera";
        mess1["content"] = "Zdravo";
        mess1["time"] = QTime::currentTime().toString();
        mess.push_back(mess1);

        ChatBackend cb(mess);

        QJsonArray izlaz = cb.convertToJson();
        REQUIRE(mess == izlaz);
    }
}

