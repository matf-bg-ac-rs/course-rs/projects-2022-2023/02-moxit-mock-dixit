#include "player.hpp"

#include <QJsonArray>

Player::Player(std::string userName, int id, int points, std::vector<int> hand){
    m_username = userName;
    m_id = id;
    m_points = points;
    m_hand = hand;
}

Player::Player(const QJsonObject& o){

    m_username = o["userName"].toString().toStdString();
    m_id = Player::playerCnt++;
    m_points = 0;


}

Player::Player(){
    Player::playerCnt++;
    m_username = "player" + std::to_string(playerCnt);
    m_id = Player::playerCnt++;
    m_points = 0;

}

std::string Player::username() const {
    return m_username;
}

int Player::id() const {
    return m_id;
}

std::vector<int> Player::cards() const{
    return m_hand;
}

int Player::points() const{
    return m_points;
}



void Player::addCard(int cardId){
    m_hand.push_back(cardId);
}

void Player::addPoints(int points){
    m_points += points;
}

void Player::discard(int cardId){
    for(int i = 0; i < m_hand.size(); i++){
        if(m_hand[i] == cardId){
            m_hand.erase(m_hand.begin() + i);
            break;
        }
    }
}

QJsonObject Player::toJson(){
    QJsonObject p;
    p["username"] = QString::fromStdString(m_username);
    p["id"] = QJsonValue(m_id);
    p["points"] = QJsonValue(m_points);
    p["color"] = QJsonValue(m_id);

    QJsonArray serializedHand;
    for(size_t i=0; i<m_hand.size(); i++)
        serializedHand.push_back(QJsonValue(m_hand[i]));
    p["hand"] = serializedHand;

    return p;
}



Player* Player::fromJson(const QJsonObject& o){
    std::string userName = o["username"].toString().toStdString();
    int id = o["id"].toInt();
    int points = o["points"].toInt();

    std::vector<int> deserializedHand;
    QJsonArray serializedHand = o["hand"].toArray();
    for(auto el : serializedHand){
        deserializedHand.push_back(el.toInt());
    }

    Player* p = new Player(userName, id, points, deserializedHand);
    return p;

}
int Player::playerCnt = -1;



/*
std::ostream &operator<<(std::ostream &out, const Player &p) {
    out << "username: " << p.username() << ", color: " << p.color() <<
    ", points: " << p.points();
    return out;
}
*/
