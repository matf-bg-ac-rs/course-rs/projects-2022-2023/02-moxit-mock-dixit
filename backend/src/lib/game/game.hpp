#ifndef GAME_HPP
#define GAME_HPP
#include "player.hpp"
#include "../chat/ChatBackend.hpp"

#include <string>
#include <cassert>
#include <vector>
#include <iostream>
#include <ctime>
#include <random>
#include <algorithm>
#include <unordered_map>
#include <stdlib.h>
#include <QJsonDocument>
#include <QByteArray>
#include <QString>
#include <QTime>

#define MAX_HANDSIZE (6)
#define MAXPOINTS (3)
#define DECKSIZE (84)
#define INITIALDEAL (6)

enum class GamePhase{
    NARRATOR_CHOICE,
    PLAYERS_CHOOSE,
    PLAYERS_VOTE
};
std::string gamePhaseToStr(GamePhase gamePhase);
GamePhase strToGamePhase(std::string strGamePhase);

class Game{
public:
    Game();

    //getters:
    GamePhase gamePhase() const;
    int gameID() const;
    int currNarrator() const;
    int numOfPlayers() const;
    int maxPlayers() const;
    std::vector<Player> players() const;
    std::string clue() const;
    int narratorCard() const;
    std::unordered_map<int, int> commonPile() const; // idKarte : idIgraca - ova karta pripada ovoj osobi
    std::unordered_map<int, int> votes(); //idIgraca : idKarte

    void setGameId(int newId);


    //adding a player to the lobby
    void addPlayer(const QJsonObject &config);
    void addPlayer(Player &p);

    //removing a player from the game
    void removePlayer(const QJsonObject &config);

    //koristi konfiguraciju (broj igraca, potencijalno tajmeri itd)
    void initializeGame(const QJsonObject &config);

    //promesaj dek, postavi prvobitnog naratora, podeli svakom igracu po 6 karata
    void startGame();


    //dodeli karte svim igracima u ruku
    void drawCards();

    //Za naratora: izbor karte i asocijacije
    void narratorChoice(int cardId, const std::string& clue);

    //mesanje spila
    void shuffleDeck();

    //Za sve osim naratora: Izbor karte koja podseca na asocijaciju
    void playerChoice(int playerId, int cardId);

    //Za sve osim naratora : glasanje za naratorovu kartu
    void playerVote(int voteFromId, int cardId);
    
    //azuriranje poena svih igraca
    void updatePoints();


    //na kraju jednog ciklusa(promeni naratora, obracunaj poene, dodeli po jednu kartu itd..)
    //pozivamo ovaj metod nakon sto poslednja osoba glasa (ili kada narator ne da asocijaciju i kartu)
    void endTurn();

    //provera da li je gotova igra
    bool checkIfOver();

    //toJson
    QJsonObject toJson();

    std::string toJsonString();

    //fromJson
    static Game* fromJson(const QJsonObject& o);

    ChatBackend m_chat;

private:

    int m_gameID;

    int m_hostId;

    int m_maxPlayers; //ono sto je host postavio u podesavanjima
    std::vector<Player> m_players;
    std::unordered_map<int, int> m_idToPlayer; //slika id igraca u njegov indeks u nizu m_players
    
    int m_currNarratorId;
    int m_currNarrator; //indeks u nizu igraca, ne njegov id
    int m_narratorCard;
    int m_prevNarratorCard;
    std::string m_clue;

    std::unordered_map<int, int> m_commonPile; // idKarte : idIgraca - ova karta pripada ovoj osobi
    std::unordered_map<int, int> m_votes; //idIgraca : idKarte

    std::vector<int> m_deck; // cardId

    int m_choiceCounter;
    int m_voteCounter;
    GamePhase m_gamePhase;

    bool m_isOver;
    bool m_isGameStarted;


    void notify(std::string info);
    void notifyPlayers();


};

std::ostream &operator<<(std::ostream &out, const Game &g);

#endif 
