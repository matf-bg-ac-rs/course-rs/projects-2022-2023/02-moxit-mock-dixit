#define CATCH_CONFIG_MAIN
#include "catch.hpp"

#define private public

#include "player.hpp"
#include "game.hpp"
#include <QFile>
#include <QJsonDocument>

QJsonObject loadJson(const QString &fpath){
    QFile file(fpath);
    if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
        return {};

    auto data = QJsonDocument::fromJson(file.readAll()).object();
    return data;
}

TEST_CASE("Test - konstruisanje objekta klase Player", "[Player]")
{
    SECTION("Za prazan konstruktor postavljaju se podrazumevane vrednosti polja m_username, m_points")
    {
        //Act
        Player p;

        //Assert
        CHECK(p.username() == "player0");
        CHECK(p.points() == 0);
    }

    SECTION("Za konstruktor koji prima username, id, points i hand, "
            "kreira se objekat klase Igrac i postavljaju se vrednosti za polja m_username, m_id, m_points")
    {
        //Act
        Player p("tiney", 123, 10, {1, 2, 3});

        //Assert
        CHECK(p.username() == "tiney");
        CHECK(p.id() == 123);
        CHECK(p.points() == 10);
        CHECK(p.cards().size() == 3);
    }
}

TEST_CASE("Dodavanje karte u ruku igraca", "[Player]")
{
    SECTION("Kada se doda karta u praznu ruku, dimenzija vektora hand postaje 1"){
        //Arrange
        Player p("tiney", 123, 0, {});

        //Act
        p.addCard(123);

        //Assert
        CHECK(p.cards().size() == 1);
    }

    SECTION("Kada se doda karta u nepraznu ruku, dimenzija vektora hand se povecava za 1")
    {
        //Arrange
        Player p("tiney", 123, 0, {1, 2, 3});

        //Act
        p.addCard(4);

        //Assert
        CHECK(p.cards().size() == 4);
    }
}

TEST_CASE("Testiranje metoda drawCard", "[Player]")
{
    SECTION("Kada se izabere karta iz ruke u kojoj se nalazi samo 1 karta, m_hand.size() postaje 0")
    {
        //Arrange
        Player p("tiney", 123, 0, {123});

        //Act
        p.discard(123);

        //Assert
        CHECK(p.cards().size() == 0);
    }

    SECTION("Kada se izabere karta iz ruke u kojoj se nalazi vise od 1 karte, m_hand.size() se smanjuje za jedan")
    {
        //Arrange
        Player p("tiney", 123, 0, {1, 2, 3});

        //Act
        p.discard(2);

        //Assert
        CHECK(p.cards().size() == 2);
    }
}

TEST_CASE("Testiranje metoda addPoints", "[Player]")
{
    SECTION("Kada se pozove metod addPoints, vrednost polja points se uvecava za odredjeni broj poena")
    {
        //Arrange
        Player p("tiney", 123, 10, {1, 2, 3});

        //Act
        p.addPoints(3);

        //Assert
        CHECK(p.points() == 13);
    }
}

TEST_CASE("Testiranje metoda fromJson", "[Player]")
{
    SECTION("Kada se pozove metod fromJson, kreira se objekat klase Player i postavljaju mu se sva polja")
    {
        //Arrange
        QJsonObject o{
            {"username", "tiney"},
            {"id", 200},
            {"points", 12},
            {"hand", QJsonArray({1, 2, 3, 4, 5, 6})}
        };

        //Act
        Player *player = Player::fromJson(o);

        //Assert
        for(int i = 0; i < 6; i++)
            CHECK(player->cards()[i] == i + 1);

        CHECK(player->username() == "tiney");
        CHECK(player->id() == 200);
        CHECK(player->points() == 12);
    }
}

TEST_CASE("Testiranje kreiranja objekta klase Game", "[Game]")
{

    SECTION("Za prazan konstruktor kreira se objekat klase Game inicijalizuju se polja"
            " m_chat, m_voteCounter, m_choiceCounter, "
            "m_gameID, m_maxPlayers, m_currNarrator, m_narratorCard, m_gamePhase. ")
    {
        //Arrange + act
        Game game;

        //Assert
        CHECK(game.currNarrator() == -1);
        CHECK(game.narratorCard() == -1);
        CHECK(game.m_gamePhase == GamePhase::NARRATOR_CHOICE);
        CHECK(game.m_voteCounter == 0);
        CHECK(game.m_choiceCounter == 0);
        CHECK(game.maxPlayers() == 3);
    }
}

TEST_CASE("Testiranje inicijalizacije partije", "[Game")
{
    SECTION("Kada se pozove metod initializeGame, postavlja se vrednost polja m_maxPlayers, m_host i vektor"
            "igrada postaje dimenzije 1")
    {
        //Arrange
        Game game;
        QJsonObject o{
            {"numOfPlayers", 3},
        };

        //Act
        game.initializeGame(o);

        //Assert
        CHECK(game.numOfPlayers() == 1);
        CHECK(game.maxPlayers() == 3);
        CHECK(game.m_deck.size() == DECKSIZE);
        CHECK(game.players().size() == 1);
        CHECK(game.m_hostId == 0);
    }
}

TEST_CASE("Testiranje metode addPlayer", "[Game]")
{
    SECTION("Kada se pozove metod addPlayer, povecava se dimenzija vektora m_players za 1")
    {
        //Arrange
        Game game;
        QJsonObject o{
            {"numOfPlayers", 3},
            {"userName", "scooby"}
        };
        game.initializeGame(o);

        Player player1(QJsonObject{
                    {"userName", "shaggy"}
               });

        Player player2(QJsonObject{
                    {"userName", "freddy"}
               });

        //Act
        game.addPlayer(player1);
        game.addPlayer(player2);

        //Assert
        CHECK(game.numOfPlayers() == 3);
    }
}

TEST_CASE("Testiranje metoda narratorChoice", "[Game]")
{
    SECTION("Igrac bira kartu i asocijaciju, koji se prosledjuju kao parametri metoda"
            "narratorChoice, nakon poziva postavljaju se polja m_narratorCard, m_clue, "
            "smanjuje se dimenzija ruke naratora, u commonPile se dodaje par <cardId, narratorId> "
            "i postavlja se vrednost flag-a gamePhase na PLAYERS_CHOOSE")
    {
        //Arrange


        auto o = loadJson("../../../src/assets/beforeNarratorChoice.json");

        Game *game= Game::fromJson(o);
        std::string clue = "scooby dooby dooooo";

        //Act
        game->narratorChoice(22, clue);

        //Assert
        CHECK(game->narratorCard() == 22);
        CHECK(game->clue() == clue );
        CHECK(game->players()[game->currNarrator()].cards().size() == 5);
        CHECK(game->commonPile().size() == 1);
        CHECK(gamePhaseToStr(game->gamePhase()) == "PLAYERS_CHOOSE");
    }

}

TEST_CASE("Biranje karte iz ruke igraca koji nije narator", "[Game]")
{
    SECTION("Kada igrac iz ruke izabere kartu, ali nisu svi izabrali kartu iz ruke, azurira se mapa m_commonPile, broj karata u ruci se smanjuje")
    {
        //Arrange
        auto o = loadJson("../../../src/assets/beforeHandChoice1.json");
        Game *game = Game::fromJson(o);

        //Act
        game->playerChoice(125, 24);

        //Assert
        CHECK(game->m_commonPile[24] == 125);
        CHECK(game->players()[game->m_idToPlayer[125]].cards().size() == 5);
    }
    SECTION("Specijalni slucaj - Kada poslednji od svih igraca iz ruke izabere kartu, "
            "azurira se mapa m_commonPile, broj karata u ruci tog igraca se smanjuje a flag"
            "m_gamePhase postaje PLAYERS_VOTE")
    {
        //Arrange
        auto o = loadJson("../../../src/assets/beforeLastHandChoice.json");
        Game *game = Game::fromJson(o);

        //Act
        game->playerChoice(123, 29);

        //Assert
        CHECK(game->m_commonPile[29] == 123);
        CHECK(game->players()[game->m_idToPlayer[123]].cards().size() == 5);
        CHECK(gamePhaseToStr(game->m_gamePhase) == "PLAYERS_VOTE");
    }
}

TEST_CASE("Glasanje igraca (koji nije narator) za kartu za koju smatra da je naratorova", "[Game, Integration]")
{
    SECTION("Kada glasa neki, ali ne svi igraci - postavlja se vrednost mape m_votes, ne menja se flag"
            "gamePhase")
    {
        //Arrange
        auto o = loadJson("../../../src/assets/beforeFirstVote.json");
        Game *game = Game::fromJson(o);

        //Act
        game->playerVote(125, 56);

        //Assert
        CHECK(game->votes()[125] == 56);
        CHECK(gamePhaseToStr(game->m_gamePhase) == "PLAYERS_VOTE");


    }
    SECTION("Specijalni slucaj - kada svi igraci (osim naratora) glasaju, poziva se metod endTurn"
            "cime se resetuje clue, prazne commonPile i votes mape, flag m_gamePhase se postavlja na"
            "NARRATOR_CHOICE, m_prevNarratorCard se postavlja na vrednost id karte naratora, svakom"
            "od igraca se dodeljuje po jedna karta u ruku, azuriraju se poeni")
    {
        //Arrange
        auto o = loadJson("../../../src/assets/beforeLastVote.json");
        Game *game = Game::fromJson(o);

        //Act
        game->playerVote(123, 56);

        //Assert

        CHECK(gamePhaseToStr(game->m_gamePhase) == "NARRATOR_CHOICE");
        CHECK(game->m_clue == "");
        CHECK(game->commonPile().size() == 0);
        CHECK(game->votes().size() == 0);

    }

}
TEST_CASE("Testiranje metode updatePoints", "[Game]")
{
    SECTION("Specijalni slucaj - kada svi igraci glasaju za naratorovu kartu, svi osim naratora dobijaju po 2p")
    {
        //Arrange
        auto o = loadJson("../../../src/assets/allVoteForNarrator.json");
        Game *game = Game::fromJson(o);

        //Act
        game->updatePoints();

        //Assert
        CHECK(game->players()[game->m_idToPlayer[125]].points() == 2);
        CHECK(game->players()[game->m_idToPlayer[123]].points() == 12);
        CHECK(game->players()[game->m_currNarrator].points() == 12);


    }

    SECTION("Specijalan slucaj - Ne glasaju svi za naratorovu kartu - onaj igrac koji glasa za naratorovu kartu,"
            " ta osoba i narator dobijaju po 3p "
            "a onaj igrac za ciju je kartu glasao neki drugi igrac dobija 1 poen")
    {
        //Arrange
        auto o = loadJson("../../../src/assets/notAllVoteForNarrator.json");
        Game *game = Game::fromJson(o);

        //Act
        game->updatePoints();

        //Assert
        CHECK(game->players()[game->m_idToPlayer[123]].points() == 10);
        CHECK(game->players()[game->m_idToPlayer[125]].points() == 4);
        CHECK(game->players()[game->m_currNarrator].points() == 15);
    }

    SECTION("Specijalni slucaj - kada niko ne glasa za naratorovu kartu, svi osim naratora dobijaju po 2p, a onaj"
            "igrac ciju je kartu izabrao neki drugi igrac dobija 1 poen")
    {

        //Arrange
        auto o = loadJson("../../../src/assets/noneVoteForNarrator.json");
        Game *game = Game::fromJson(o);

        //Act
        game->updatePoints();

        //Assert
        CHECK(game->players()[game->m_idToPlayer[125]].points() == 3);
        CHECK(game->players()[game->m_idToPlayer[123]].points() == 13);
        CHECK(game->players()[game->m_currNarrator].points() == 12);
    }
}




