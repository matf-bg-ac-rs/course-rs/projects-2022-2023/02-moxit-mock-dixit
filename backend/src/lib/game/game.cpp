#include "game.hpp"

Game::Game() : m_chat(QJsonArray()){
    m_currNarratorId = -1;
    m_maxPlayers = 3;
    m_gameID = time(NULL);
    m_currNarrator = -1;
    m_narratorCard = -1;
    m_gamePhase = GamePhase::NARRATOR_CHOICE;
    m_voteCounter = 0;
    m_choiceCounter = 0;
    m_hostId = 0;
    m_isOver = false;
    m_prevNarratorCard = -1;
    m_isGameStarted = false;
    m_clue ="";
}

GamePhase Game::gamePhase() const{
    return m_gamePhase;
}

int Game::gameID() const{
    return m_gameID;
}

int Game::currNarrator() const{
    return m_currNarrator;
}

int Game::numOfPlayers() const{
    return m_players.size();
}

int Game::maxPlayers() const{
    return m_maxPlayers;
}

std::vector<Player> Game::players() const{
    return m_players;
}

std::string Game::clue() const{
    return m_clue;
}

int Game::narratorCard() const{
    return m_narratorCard;
}

std::unordered_map<int, int> Game::commonPile() const{
    return m_commonPile;
}

std::unordered_map<int, int> Game::votes(){
    return m_votes;
}

void Game::shuffleDeck(){
    int n = m_deck.size();
    srand(time(NULL)); //todo globalni seed

    for (int i = n - 1; i > 0; i--)
    {
        int j = rand() % (i + 1);

        std::swap(m_deck[i], m_deck[j]);
    }
}

void Game::initializeGame(const QJsonObject& config){

    m_maxPlayers =  config["numOfPlayers"].toInt();
    //todo : dodati tajmere

    //inicijalizuj id-eve decka
    m_deck.resize(DECKSIZE);
    for(int i = 0; i < DECKSIZE; i++){
        m_deck[i] = i + 1;
    }

    //dodati host-a:
    addPlayer(config);
    m_hostId = m_players[0].id();

}

void Game::addPlayer(Player& p){

    m_players.push_back(p);
    m_idToPlayer[p.id()] = m_players.size() - 1;

}

void Game::addPlayer(const QJsonObject &config){

    std::string username = config["userName"].toString().toStdString();
    Player p(username, m_players.size(), 0, {});
    m_players.push_back(p);
    m_idToPlayer[p.id()] = m_players.size() - 1;


}
void Game::removePlayer(const QJsonObject &config){

    int id = config["id"].toInt();
    //std::string userName = config["username"].toString().toStdString();

    for(int i = 0; i < m_players.size(); i++){
        if(m_players[i].id() == id){
            m_players.erase(m_players.begin() + i);
            break;
        }
    }
}


void Game::startGame(){

    m_isGameStarted = true;
    shuffleDeck();
    m_currNarrator = rand() % numOfPlayers();
    m_currNarratorId = players()[m_currNarrator].id();

    for(int i = 0; i < INITIALDEAL; i++)
        drawCards();

    const std::string msg = "Igra je pocela. Narator je " + m_players[m_currNarrator].username() + ".";
    notify(msg);


}

//ovo bi moglo da bude private
void Game::drawCards(){

    for(int i = 0; i < m_players.size(); i++){
        m_players[i].addCard(m_deck.back());
        m_deck.pop_back();
    }
}

void Game::narratorChoice(int cardId, const std::string& clue){

    std::vector<int> hand = m_players[m_currNarrator].cards();
    assert(std::count(hand.begin(), hand.end(), cardId) > 0);

    //postavi asocijaciju i naratorovu kartu
    m_clue = clue;
    m_narratorCard = cardId;

    //dodaj u commonPile
    m_commonPile[m_narratorCard] = m_currNarratorId;

    //izbaci iz ruke
    m_players[m_currNarrator].discard(cardId);

    //promeni gamePhase
    m_gamePhase = GamePhase::PLAYERS_CHOOSE;

    //obavesti ostale da biraju kartu iz ruke
    std::string msg = "Odaberite kartu iz ruke.";
    notify(msg);
}

void Game::playerChoice(int playerId, int cardId){

    std::vector<int> hand = m_players[m_idToPlayer[playerId]].cards();
    assert(std::count(hand.begin(), hand.end(), cardId) > 0);

    m_commonPile[cardId] = playerId;

    m_players[m_idToPlayer[playerId]].discard(cardId);

    for(int i = 0; i < m_players.size(); i++){
        if(m_players[i].id() == playerId){
           std::string msg = "Igrac " + m_players[i].username() + " je izabrao kartu iz ruke.";
           notify(msg);
        }
    }

    m_choiceCounter++;

    if(m_choiceCounter == m_players.size() - 1){
        m_gamePhase = GamePhase::PLAYERS_VOTE;
        m_choiceCounter = 0;
        std::string msg = "Vreme je za glasanje.";
        notify(msg);
    }
}

void Game::playerVote(int playerId, int cardId){

    assert(m_commonPile.find(cardId) != m_commonPile.end());
    m_votes[playerId] = cardId;

    for(int i = 0; i < m_players.size(); i++){
        if(m_players[i].id() == playerId){
           std::string msg = "Igrac " + m_players[i].username() + " je glasao.";
           notify(msg);
        }
    }
    m_voteCounter++;
    if(m_voteCounter == m_players.size() - 1){
        m_voteCounter = 0;

        endTurn();
    }
}

void Game::updatePoints(){

    int forNarrator = 0;

    for(int i = 0; i < m_players.size(); i++){
        //ne gledamo za koga je narator glasao, iako svakako ne glasa
        if(i == m_currNarrator)
            continue;

        int chosenCard = m_votes[m_players[i].id()];

        //ako je igrac glasao za naratora
        if(chosenCard == m_narratorCard)
            forNarrator++;
        //ako nije glasao za naratora nego za nekog drugog, taj drugi dobija poene
        else {
           int pointsFor = m_commonPile[chosenCard];
           m_players[m_idToPlayer[pointsFor]].addPoints(1);
        }
    }
    int pointsForPlayer = MAXPOINTS;
    bool givePointsToNarrator = true;

    //da li su svi glasali za naratora?
    if(forNarrator == m_players.size() - 1 || forNarrator == 0){
        pointsForPlayer--;
        givePointsToNarrator = false;
    }
    //dodela svima osim naratoru
    for(int i = 0; i < m_players.size(); i++){
        if(i == m_currNarrator){
           continue;
        }
        else if(m_votes[m_players[i].id()] == m_narratorCard){
            m_players[i].addPoints(pointsForPlayer);
            if(givePointsToNarrator)
                m_players[m_currNarrator].addPoints(pointsForPlayer);
        }
        else if(!givePointsToNarrator){
            m_players[i].addPoints(pointsForPlayer);
        }
    }
}
void Game::notify(std::string message){

    QJsonObject messageInfo;
    messageInfo["time"] = QTime::currentTime().toString();
    messageInfo["userName"] = QString("Server");
    messageInfo["content"] = QString::fromStdString(message);

    m_chat.addMessage(messageInfo);

}

void Game::notifyPlayers(){

    for(int i = 0; i < m_players.size(); i++){
        std::string msg = "Igrac " + m_players[i].username() + " ima " + std::to_string(m_players[i].points()) + " poena.";
        notify(msg);
    }

    std::string msg = "Novi narator je " + m_players[m_currNarrator].username() + ".";
    notify(msg);
}

void Game::endTurn(){

    updatePoints();
    m_isOver = checkIfOver();
    drawCards();

    m_currNarrator = (m_currNarrator+1) % m_players.size();
    m_currNarratorId = m_players[m_currNarrator].id();

    m_commonPile.clear();
    m_votes.clear();
    m_gamePhase = GamePhase::NARRATOR_CHOICE;
    m_clue = "";
    m_prevNarratorCard = m_narratorCard;
    m_narratorCard = -1;

    notifyPlayers();
}

bool Game::checkIfOver(){

    bool check = false;

    if(m_deck.empty()){
        std::string msg = "Iskoriscene su sve karte iz deck-a. Igra je zavrsena.";
        notify(msg);
        return true;
    }
    else{
        for(int i = 0; i < m_players.size(); i++){
            if(m_players[i].points() >= 30){
                std::string msg = "Pobednik je " + m_players[i].username() + ".";
                notify(msg);
                check = true;
            }
        }
        return check == true;
    }
}

QJsonObject Game::toJson(){

    QJsonObject p;

    p["voteCounter"] = QJsonValue(m_voteCounter);
    p["choiceCounter"] = QJsonValue(m_choiceCounter);
    p["isGameStarted"] = QJsonValue(m_isGameStarted);
    p["prevCard"] = QJsonValue(m_prevNarratorCard);
    p["isOver"] = QJsonValue(m_isOver);
    p["id"] = QJsonValue(m_gameID);
    p["hostId"] = QJsonValue(m_hostId);
    p["gamePhase"] = QString::fromStdString(gamePhaseToStr(m_gamePhase));

    QJsonObject serializedPlayers;
    for(size_t i = 0; i < m_players.size(); i++){
        auto playerId = QString::number(m_players[i].id());
        serializedPlayers[playerId] = m_players[i].toJson();
    }
    p["players"] = serializedPlayers;

    if(m_players.size() > 0 && m_isGameStarted == true){

        p["narratorId"] = QJsonValue(m_players[m_currNarrator].id());
    }


    p["currNarrator"] = QJsonValue(m_currNarrator);
    p["clue"] = QString::fromStdString(m_clue);
    p["narratorCard"] = QJsonValue(m_narratorCard);

    QJsonObject serializedCommonPile;
    for(auto &p : m_commonPile){
        auto cardId = QString::number(p.first);
        auto playerId = QJsonValue(p.second);
        serializedCommonPile[cardId] = playerId;
    }
    p["commonPile"] = serializedCommonPile;


    QJsonObject serializedVotes;
    for(auto &p : m_votes){
        auto playerId = QString::number(p.first);
        auto cardId = QJsonValue(p.second);
        serializedVotes[playerId] = cardId;
    }
    p["votes"] = serializedVotes;


    QJsonArray serializedDeck;
    for(size_t i = 0; i < m_deck.size(); i++){
        auto cardId = m_deck[i];
        serializedDeck.append(cardId);
    }
    p["deck"] = serializedDeck;

    QJsonArray serializedChat = m_chat.convertToJson();
    p["chat"] = serializedChat;
    p["maxPlayers"] = QJsonValue(m_maxPlayers);

    return p;


}
Game* Game::fromJson(const QJsonObject& o){

    int prevNarratorCard = o["prevCard"].toInt();

    int voteCounter = o["voteCounter"].toInt();
    int choiceCounter = o["choiceCounter"].toInt();
    bool isOver = o["isOver"].toBool();
    int id = o["id"].toInt();
    int hostId = o["hostId"].toInt();
    int maxPlayers = o["maxPlayers"].toInt();
    GamePhase gamePhase = strToGamePhase(o["gamePhase"].toString().toStdString());


    std::vector<Player> deserializedPlayers;
    QJsonObject serializedPlayers = o["players"].toObject();
    for(auto &key: serializedPlayers.keys()){
        auto temp = Player::fromJson(serializedPlayers[key].toObject());
        deserializedPlayers.push_back(*temp);
    }

    int currNarrator = o["currNarrator"].toInt();
    int narratorCard = o["narratorCard"].toInt();
    std::string clue = o["clue"].toString().toStdString();

    std::unordered_map<int, int> deserializedCommonPile;
    QJsonObject serializedCommonPile = o["commonPile"].toObject();
    for(auto it = serializedCommonPile.begin(); it != serializedCommonPile.end(); ++it){
        int cardId = it.key().toInt();
        int playerId = it.value().toInt();
        deserializedCommonPile[cardId] = playerId;
    }

    std::unordered_map<int, int> deserializedVotes;
    QJsonObject serializedVotes = o["votes"].toObject();
    for(auto it = serializedVotes.begin(); it != serializedVotes.end(); ++it){
        int playerId = it.key().toInt();
        int cardId = it.value().toInt();
        deserializedVotes[playerId] = cardId;
    }

    std::vector<int> deserializedDeck;
    QJsonArray serializedDeck = o["deck"].toArray();
    for(int i = 0; i < serializedDeck.size(); i++){
        int val = serializedDeck[i].toInt();
        deserializedDeck.push_back(val);
    }

    Game *g = new Game();
    g->m_maxPlayers = maxPlayers;
    g->m_gameID = id;
    for(int i = 0; i < deserializedPlayers.size(); i++){
        g->addPlayer(deserializedPlayers[i]);
    }
    int narratorId = o["narratorId"].toInt();

    bool isGameStarted = o["isGameStarted"].toBool();
    g->m_currNarratorId = narratorId;
    g->m_isOver = isOver;
    g->m_currNarrator = currNarrator;
    g->m_narratorCard = narratorCard;
    g->m_clue = clue;
    g->m_commonPile = deserializedCommonPile;
    g->m_votes = deserializedVotes;
    g->m_deck = deserializedDeck;
    g->m_hostId = hostId;
    g->m_gamePhase = gamePhase;
    g->m_maxPlayers = maxPlayers;
    g->m_prevNarratorCard = prevNarratorCard;
    g->m_isGameStarted = isGameStarted;
    g->m_voteCounter = voteCounter;
    g->m_choiceCounter = choiceCounter;

    return g;
}

std::string gamePhaseToStr(GamePhase gamePhase)
{
    switch (gamePhase) {
        case GamePhase::NARRATOR_CHOICE :  return "NARRATOR_CHOICE";
        case GamePhase::PLAYERS_VOTE    :  return "PLAYERS_VOTE";
        case GamePhase::PLAYERS_CHOOSE  :  return "PLAYERS_CHOOSE";

        default:
            break;
    }
    return "";
}
GamePhase strToGamePhase(const std::string strGamePhase){

    if(strGamePhase == "NARRATOR_CHOICE")
        return GamePhase::NARRATOR_CHOICE;
    else if(strGamePhase == "PLAYERS_VOTE")
        return GamePhase::PLAYERS_VOTE;
    else if(strGamePhase == "PLAYERS_CHOOSE")
        return GamePhase::PLAYERS_CHOOSE;
    else
        return GamePhase::NARRATOR_CHOICE; //proveriti sta u ovom slucaju!
}


std::string Game::toJsonString(){
    QJsonDocument doc = QJsonDocument(toJson());
    QByteArray data = QByteArray(doc.toJson());
    std::string dataString = data.toStdString();
    return dataString;
}

void Game::setGameId(int newId){
    m_gameID = newId;
}
