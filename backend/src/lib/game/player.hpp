#ifndef USER_HPP
#define USER_HPP

#include <string>
#include <iostream>
#include <vector>
#include <QJsonObject>

#include <unordered_map>


class Player{
public:
    Player(std::string userName, int id, int points, std::vector<int> hand);
    Player(const QJsonObject& o);

    Player();

    // getters
    std::string username() const;
    //std::string color() const; //mozda suvisno
    int points() const;
    int id() const;
    std::vector<int> cards() const;

    //dodaj kartu u ruku
    void addCard(int cardId);

    //azuriraj poene
    void addPoints(int points);

    //izbaci kartu iz ruke
    void discard(int cardId);

    //toJson
    QJsonObject toJson();

    //fromJson
    static Player* fromJson(const QJsonObject& o);

private:

    std::string m_username;
    int m_id;
    std::vector<int> m_hand;
    int m_points;
    //Color m_color;
    static int playerCnt;



};
std::ostream &operator<<(std::ostream &out, const Player &p);


#endif 
